function initPrototype() {
	(function() {
		var bodyObj,cancelPopup,containerMidpoint,currentPopupObj,detailRow,exitWithoutSavingPopup,GETmethod,headerRow,helpPopup,helpWidth,isTablet,messageKey,pht,popupFundProfile,popupLeaving,popupSaveReturn,popupTitle,printDropDown,tHideActionsMenu,timeoutValue,toggleShortcuts,tooltip,ua,url,popupWait;

		bodyObj = $(document.body);

		// demo data
		bodyObj.attr('data-demo','body');

		// tablet environment
		ua = navigator.userAgent;
		isTablet = (/android|ipad|playbook|webos/gi).test(ua);

		function toggleAttribute(thisObj,attrNames) {
			"use strict";
			var i,names,namesl;
			names = attrNames.split(' ');
			namesl = names.length;
			for(i=0;i<namesl;i++)
			{
				thisObj.attr(names[i],thisObj.attr(names[i]) === 'false' ? 'true' : 'false');
			}
		}

		function queryEmpty(queryObj,queryText) {
			"use strict";
			if(queryObj.val() === '')
			{
				queryObj.val(queryText);
				queryObj.addClass('q');
			}
		}

		// actionsMenu
		tHideActionsMenu = null;

		function hideActionsMenu() {
			clearTimeout(tHideActionsMenu);
			$('a.actions.active').removeClass('active');
			$('div.actionsMenu.active').removeClass('active').fadeTo('slow',0,function(){
				$(this).hide().css({left:'-100px',top:'-100px'});
			});
		}

		function showActionsMenu(link) {
			var actionsMenuWidth,leftPos,menu,thisPos,topPos;
			link = $(link);
			menu = $($(link).attr('rel'));

			link.blur();
			hideActionsMenu();

			if(menu.length > 0)
			{
				link.addClass('active');
				thisPos = link.offset();
				topPos = thisPos.top + link.height() + 1;
				actionsMenuWidth = menu.width();
				leftPos = thisPos.left - actionsMenuWidth + link.width() + 28;

				if(menu.css('opacity') < 0.01) { menu.show(); }

				menu.stop(true, true)
				.addClass('active')
				.css({
					'top':topPos + 'px',
					'left':leftPos + 'px'
				})
				.fadeTo('slow', 1);
			}
		}

		// framebreaker (inherited from earlier standard code)
		/* Removed by Travis - if this is needed please contact
		   me or Vic first */
		// if(top.location.href != window.location.href) {
		// 	top.location.href = window.location.href;
		// }

		if($('header').hasClass('noNav')) { $('#skip').remove(); }
		else
		{
			bodyObj.children(':first').before('<div id="skip"><a href="#pagecontent">Skip to Main Content</a></div>');
			$('div.pagecontent:first').attr({
				'id':'pagecontent',
				'role':'main',
				'tabindex':-1
			});
		}

		if(isTablet === true)
		{
			$('input[type="text"]').on('click',function() {
				this.setSelectionRange(0,99999);
			}).on('mouseup',function(event){
				event.preventDefault();
			});
		}
		else
		{
			$('input[type="text"]').on('click',function() {
				$(this).select();
			});
		}

		// maxlength behaviors
		if(isTablet === false) {
			$('input.tabnext').on('keyup',function() { // tab to next field when maxlength reached
				var thisObj = $(this);
				if(thisObj.val().length === Number(thisObj.attr('maxlength')))
				{
					thisObj.nextAll('input:first').focus();
				}
			});
		}

		if(!Modernizr.input.placeholder)
		{
			$('input[placeholder],textarea[placeholder]').focus(function() {
				var input = $(this);
				if(input.val() === input.attr('placeholder'))
				{
				input.val('');
				input.removeClass('placeholder');
				}
			}).blur(function() {
				var input = $(this);
				if(input.val() === '' || input.val() === input.attr('placeholder'))
				{
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
				}
			}).blur();

			$('input[placeholder],textarea[placeholder]').parents('form').submit(function() {
				$(this).find('input[placeholder],textarea[placeholder]').each(function() {
					var input = $(this);
					if(input.val() === input.attr('placeholder'))
					{
						input.val('');
					}
				})
			});
		}

		/*
		 * Sometimes there's a requirement to display specific placeholder text,
		 * so let's check for the existence of a placeholder attribute before defaulting to the current date
		 */

		$('input.inputDate').val((typeof $('input.inputDate').attr('placeholder') === 'undefined') ? Date.today().toString('MM/dd/yyyy') : $('input.inputDate').attr('placeholder'));
		$('span.currentYYYY').html(Date.today().toString('yyyy'));
		$('span.lastYYYY').html(Date.today().add(-1).year().toString('yyyy'));

		// input field number handlers; use of keyup more aggressive than blur alone
		$('input.date').on('blur',function(event) {
		  var filtered = $(this),filteredVal;
		  filteredVal = filtered.val();
		  if(filteredVal !== '')
			{ filtered.val(getDateValue(filteredVal,'date')); }
		});
		$('input.month').on('blur',function(event) {
		  var filtered = $(this),filteredVal;
		  filteredVal = filtered.val();
		  if(filteredVal !== '')
			{ filtered.val(getDateValue(filteredVal,'month')); }
		});
		$('input.year').on('blur', function(event) {
			var filtered = $(this);
			filtered.val(getDateValue(filtered.val(),'year'));
		});
		$('input.ssn').on('blur', function(event) {
			var filtered = $(this);
			filtered.val(getSSN(filtered.val(),'ssn'));
		});
		$('input.dollars').on('blur', function(event) {
			var filtered = $(this);
			filtered.val(getNumber(filtered.val(),'dollars'));
		});
		$('input.dollarsEntered').on('blur',function(event) {
			var filtered = $(this);
			filtered.val(getNumber(filtered.val(),'dollarsEntered'));
		});
		$('input.dollarsWhole').on('blur', function(event) {
			var filtered = $(this);
			filtered.val(getNumber(filtered.val(),'dollarsWhole'));
		});
		$('input.numeric').on('blur keyup', function(event) {
			var filtered = $(this);
			filtered.val(getNumber(filtered.val(),'numeric'));
		});
		$(".numericOnly").keydown(function(event) {
			if (jQuery.inArray(event.keyCode,[46,8,9,27,13,190]) !== -1 ||
	             // Allow: Ctrl+A
	             (event.keyCode == 65 && event.ctrlKey === true) ||
	             // Allow: home, end, left, right
	             (event.keyCode >= 35 && event.keyCode <= 39)) {
	                 // let it happen, don't do anything
	             return;
	         }
	         else {
	            // Ensure that it is a number and stop the keypress
	            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	            	event.preventDefault();
	            }
	        }
		});
		$('input.percent').on('blur keyup', function(event) {
			var filtered = $(this);
			filtered.val(getNumber(filtered.val(),'percent'));
		});
		$('input.percentUnrounded').on('blur keyup',function(event) {
			var filtered = $(this);
			filtered.val(getNumber(filtered.val(),'percentUnrounded'));
		});
		$('input.percentWhole').on('blur keyup',function(event) {
			var filtered = $(this);
			filtered.val(getNumber(filtered.val(),'whole'));
		});
		$('input.whole').on('blur', function(event) {
			var filtered = $(this);
			filtered.val(getNumber(filtered.val(),'whole'));
		});
		$('input.postalcode').on('blur keyup',function(event) {
			$(this).val(String($(this).val()).replace(/[^0-9a-zA-Z ]+/g,''));
		});
		$('input.zipcode').on('blur keyup', function(event) {
			var filtered = $(this);
			filtered.val(getNumber(filtered.val(),'zipcode'));
		});
		$('input.tabnext').on('click', function(event) {
			var thisObj = $(this);
			if(thisObj.val().length == thisObj.attr('maxlength')) {
				thisObj.next('input').focus();
			}
		});
		$('input.maxlength').on('click', function(event) {
			var thisObj = $(this);
			if(thisObj.val().length == thisObj.attr('maxlength')) {
				thisObj.blur();
			}
		});
		$('input.email').on('keyup',function() {
			var emailConfirm,emailVal,thisId;
			thisId = $(this).attr('id').replace(/Confirm/,'');
			emailVal = $('#'+thisId).val();
			emailConfirm = $('#'+thisId+'Confirm');
			if(emailVal !== '' && emailVal === emailConfirm.val())
			{
				emailConfirm.parent().find('div.alertHighlight').removeClass('alertHighlight');
				$('#'+thisId).parent().find('div.alertHighlight').removeClass('alertHighlight');
				emailConfirm.next().addClass('check');
			}
			else
			{
				emailConfirm.next().removeClass('check');
			}
		});
		$('input[type="text"][data-validation="zipcode"]').on('keydown',function(event){
			if(event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 13 || event.keyCode === 27 || event.keyCode === 46 || (event.keyCode === 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39))
			{return;}

			if(event.altKey || event.shiftKey || ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )))
			{ event.preventDefault(); }
		});
		$('input[type="text"][data-validation="zipcode"]').on('keyup',function(){
			$(this).val(String($(this).val()).replace(/[^0-9]+/g,''));
		});

		$('input[type="text"][data-validation="employeeid"]').on('keydown',function(event){
			if(event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 13 || event.keyCode === 27 || event.keyCode === 46 || (event.keyCode === 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39) || ((event.keyCode > 57 && event.keyCode < 91) && event.shiftKey === true))
			{return;}

			if(event.altKey || event.shiftKey || ((event.keyCode < 48 || event.keyCode > 91) && (event.keyCode < 96 || event.keyCode > 105 )))
			{ event.preventDefault(); }
		}).on('keyup',function(){
			$(this).val(String($(this).val()).replace(/[^0-9a-zA-Z]+/g,''));
		});

		$('input[type="text"][data-validation="dollarsEntered"]').on('input',function(event){
			var mantissa,thisObj,thisVal;
			thisObj = $(this);
			thisVal = thisObj.val();
			thisObj.val(String(thisVal).replace(/[^0-9,.]+/g,''));
			mantissa = thisVal.split('.')[1] || '';
			if(Number(mantissa) > 99)
			{
				thisObj.val(thisObj.data('keydownValue'));
				event.preventDefault();
				return;
			}
		}).on('keydown',function(event){
			var filtered,filteredVal,thisObj,thisVal;
			thisObj = $(this);
			thisVal = thisObj.val();
			thisObj.data('keydownValue',thisVal);

			if(event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 13 || event.keyCode === 27 || event.keyCode === 46 || (event.keyCode === 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39))
			{return;}

			if(event.altKey || event.shiftKey || (((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) && ((event.keyCode !== 110 || event.keyCode !== 188 || event.keyCode !== 190) && thisVal.indexOf('.') !== -1)))
			{ event.preventDefault(); }

			filtered = $(this);
			filteredVal = String(filtered.val()).replace(/^[0]+/g,'');
			filtered.val(filteredVal);
		});

		// search
		if($('div.globalsearch').size() > 0)
		{
			$('#pptsearch').on('focus',function() {
				var thisSearch = $(this);
				if(thisSearch.val() === thisSearch.attr('title'))
				{
					thisSearch.val('');
					thisSearch.removeClass('q');
				}
			}).on('blur',function() {
				var thisSearch = $(this);
				queryEmpty(thisSearch,thisSearch.attr('title'));
				return false;
			});
		}

		// header search
		$('header div.searchBox form').submit(function() {
			alert('Search is currently unavailable.');
			return false;
		});
		$('header div.searchBox input').css('fontStyle','normal');

		// links
		$('a.actions').on('click keypress',function(event){
			var thisAction,thisClick;
			if(event.type !== 'keypress' || event.which === 13)
			{
				event.preventDefault();
				thisClick = $(this);
				if(thisClick.is('span')) { thisAction = thisClick.closest('a'); }
				else { thisAction = thisClick; }
				showActionsMenu(thisAction);
			}
		});

		$('body').on('mouseleave','div.actionsMenu,a.actions.active',function(){
			tHideActionsMenu = window.setTimeout(function(){hideActionsMenu();},500);
		});

		$('a.actions.active,div.actionsMenu').on('mouseenter',function(){
			clearTimeout(tHideActionsMenu);
		});

		$('div.actionsMenu a').on('click',function(event){
			var thisClick = $(event.target);
			if(thisClick.attr('href') === '#')
			{
				event.preventDefault();
				thisClick.blur();
				return false;
			}
			hideActionsMenu();
		});

		// print link
		printDropDown = '<div class="small actionsMenu hidden" style="display:none;" id="printMenu"><span id="actionsValue" class="hidden"></span><ul><li id="excel" class="first"> <a href="#printWhole">Whole Page</a></li><li id="csv"><a href="#printFriendly">Print Friendly Report</a></li></ul></div>';
		bodyObj.append(printDropDown);

		$('a.printLink').attr('title','Print this page.').attr('rel','#printLink').attr('role','button');

		$('#new-homepage a.printLink').attr('rel','#printMenu');

		bodyObj.on('click keypress','a[href="#printMenu"]',function(event){
			if(event.type !== 'keypress' || event.which === 13)
			{
				event.preventDefault();
				$('#printMenu').removeClass('hidden');
				showActionsMenu($(this));
			}
		});

		bodyObj.on('click keypress','#printMenu a',function(event){
			if(event.type !== 'keypress' || event.which === 13)
			{
				event.preventDefault();
				switch($(this).attr('href'))
				{
					case '#printWhole':
						if(window.print) { window.print(); }
						else{ alert("The print feature is not available from your browser."); }
						break;
					case '#printFriendly':
						url = '../../integrated/placeholder.html?pht=Printer+Friendly+Version';
						window.open(url,'_blank','left=0,menubar=no,resizable=no,scrollbars=yes,status=no,width=760,height=500');
						break;
					default:
						window.print();
						break;
				}
			}
		});

		bodyObj.on('click keypress','a.printBtn,a[rel="#printLink"]',function(event){
			if(window.print)
			{
				if(event.type !== 'keypress' || event.which === 13)
				{
					event.preventDefault();
					$(this).blur();
					window.print();
				}
			}
			else
			{
				alert("The print feature is not available from your browser.");
			}
		});

		// download link
		bodyObj.on('click keypress','a.download,a[href="#download"]',function(event){
			if(event.type !== 'keypress' || event.which === 13)
			{
				event.preventDefault();
				$(this).blur();
				alert("This button simulates a download.");
			}
		});

		// disabled link
		$('a.disabled').on('click keypress',function(event){
			if(event.type !== 'keypress' || event.which === 13)
			{
				var thisObj = $(this);
				event.preventDefault();
				thisObj.blur();
			}
		});

		// help link
		$('a[href="#help"]').on('click keypress',function(event){
			if(event.type !== 'keypress' || event.which === 13)
			{
				event.preventDefault();
				$(this).blur();
				alert("Help is currently not available.");
			}
		});

		// in-paragraph expand/collapse
		$('#container a[href="#showHide"]').attr('title','Select to expand or collapse content below.');
		$('#container').on('click keypress','a[href="#showHide"]',function(event){
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			var thisClick = $(this);
			thisClick.addClass('hidden').blur().prev().slideToggle(400,function(){thisClick.find('span').toggleClass('hidden');thisClick.removeClass('hidden');});
			}
		});

		// multiple paragraph expand/collapse
		$('#container a[href="#showMore"]').attr('title','Select to show more content below.');
		$('#container a[href="#hideMore"]').attr('title','Select to hide content below.');
		$('#container').on('click keypress','a[href="#showMore"]',function(event){
		  var thisClick,thisDiv;
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			thisClick = $(this);
			thisDiv = thisClick.closest('div');
			thisDiv.hide(0,function(){thisDiv.next('div').slideToggle(400);});
			}
		});
		$('#container').on('click keypress','a[href="#hideMore"]',function(event){
		  var thisClick,thisDiv;
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			thisClick = $(this);
			thisDiv = thisClick.closest('div');
			thisDiv.slideToggle(400,function(){thisDiv.prev('div').show(0);});
			}
		});

		// tab navigation
		$('ul.tabs,ul.hnavList').on('click','li,a',function(event) { // entire tab clickable
		  var thisA,thisClick,thisId;
		  thisClick = $(this);
		  if(thisClick.parents('div').hasClass('copyright')) { return; } // R@W footer links
		  event.preventDefault();
		  if(thisClick.is('a')) { thisClick.blur();return; } // li handles behavior
		  thisA = thisClick.find('a');
		  if(thisA.is('.selected,.blocked')) { return; }

		  if(thisClick.closest('ul').hasClass('tabs'))
			{
			thisClick.parent().find('a,li').removeClass('selected unclickable');
			thisClick.addClass('selected'); // removes cursor:pointer
			thisA.addClass('selected unclickable');

			thisId = thisA.attr('href');
			thisClick.closest('ul').parent().find('>div[data-prototype="panel"]').addClass('hidden');
			$(thisId).removeClass('hidden').css('zoom',0).css('zoom',1); // css zoom addresses ie6/7 background issue
			}
		  else
			{
			thisClick.parent().find('a,li').removeClass('selected unclickable');
			thisClick.addClass('selected'); // removes cursor:pointer
			thisA.addClass('selected unclickable');

			thisId = thisA.attr('href');
			thisClick.closest('ul').parent().find('div[data-prototype="panel"]').addClass('hidden');
			$(thisId).removeClass('hidden').css('zoom',0).css('zoom',1); // css zoom addresses ie6/7 background issue
			}
		});

		// li expand/collapse
		$('li a.collapsed,li a.expanded,a.jq-expand-collapse').on('click.globalExpandCollapse',function(event) {
		  var thisObj = $(this),thisLi;
		  event.preventDefault();
		  thisObj.blur();
		  thisObj.toggleClass('expanded collapsed');
		  if(thisObj.hasClass('expanded')){ thisObj.find('span.icon').html('-'); }
		  else { thisObj.find('span.icon').html('+'); }
		  thisLi = thisObj.closest('li');
		  thisLi.toggleClass('bgexpanded');
		  toggleAttribute(thisLi,'aria-expanded aria-selected');
		  $(thisObj.attr('href')).slideToggle('fast');
		});

		// table expand/collapse
		$('table a.expandCollapse').attr('title','Select to expand or collapse content below.');
		$('table').on('click keypress','a.expandCollapse',function(event) {
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			var thisClick = $(this);
			thisClick.toggleClass('expanded collapsed');
			headerRow = thisClick.closest('tr');
			detailRow = $(thisClick.attr("href"));
			detailRow.toggle('fast',function(){
			  detailRow.find('td').first().toggleClass('bgExpandedBottom');
			  headerRow.find('td').toggleClass('bgExpandedTop');
			});
			}
		});

		// list expand/collapse
		// hide all li elements with index > 3 and toggle them when the show more link is clicked
		// ***
		// *** NOTE: 12/19/13 - JW: This code looks to be an updated copy of initShowMoreLink in tc_widgets... Why no merge????
		// *** Removed this. TCW already does this; However, we'll leave the additional event bindings to allow for keypress activation, etc. (TCW doesn't currently have this)
		// ***

		// $('div.more-less').each(function(e) {
		// 	var link,navlist;
		// 	navlist = $(this);

		// 	  $('li:gt(2)',navlist).hide();
		// 	  link = $('<a href="#" class="toggle addLink"><span class="icon"></span>Show more</a>');
		// 	  navlist.append(link);
		// 	  navlist.addClass('collapsed');

		// });


		$('div.more-less a.toggle').on('click keypress',function(event) {
		  var iconClass,linkText,ml,thisClick;
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			event.stopPropagation();
			thisClick = $(this);
			thisClick.blur();

			ml = thisClick.closest('div.more-less');
			ml.toggleClass('collapsed expanded');

			linkText = 'show more';
			iconClass = 'addLink';
			if(ml.hasClass('expanded'))
			  {
			  $('li', ml).show();
			  linkText = 'show less';
			  iconClass = 'minusLink';
			  }
			else
			  {
			  $('li:gt(2)', ml).hide();
			  }
			thisClick.removeClass('addLink minusLink').addClass(iconClass);
			thisClick.html("<span class='icon'></span>" + linkText);
			}
		});

		// input:radio expand/collapse
		$('input:radio.toggle').on('click',function() {
		  var thisClick,thisDiv;
		  thisClick = $(this);
		  thisDiv = $('#'+thisClick.attr('name')); // <input name="NAME" => <div id="NAME"
		  if(thisClick.hasClass('expand')) // <input class="toggle expand"
			{ thisDiv.slideDown(); }
		  else // <input class="toggle"
			{ thisDiv.slideUp(); }
		});

		// ++++++++++++++++++++++++
		// message module (question mark after h2 that expands and collapses)
		$('#container').on('click keypress','a.showHideMessageModule',function(event){
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			var evEl = event.target;
			evEl.blur();
			messageKey = evEl.id.replace(/[0-9]+/g,''); // allows for duplicate links
			showHideMessageModule($('#'+messageKey));
			}
		});

		$('div.panels').on('mouseenter','div.hd',function(){
		  $(this).addClass('hdhover');
		  }).on('mouseleave','div.hd',function(){
			$(this).removeClass('hdhover');
		  });

		$('div.panels').on('click keypress','div.hd',function(event) {
		  var thisA,thisExpandable,thisObj;
		  if(event.type !== 'keypress' || event.which === 13) // allow tabbing
			{
			// allow other clickable elements in head
			if(event.target.nodeName.match(/label|input/i)) { return; }
			if(!$(event.target).is('.collapsed,.collapsed em,.expanded,.expanded em,span.icon,div.hd,h3')) { return; }

			event.preventDefault();
			thisObj = $(this);
			thisA = thisObj.find('a');
			thisA.toggleClass('collapsed expanded');
			if(thisA.hasClass('expanded')){ thisA.find('span.icon').html('Hide '); }
			else { thisA.find('span.icon').html('Show '); }
			toggleAttribute(thisA,'aria-expanded');
			thisObj.toggleClass('hdexpanded');
			thisExpandable = thisObj.closest('div.hd').next();
			thisExpandable.toggleClass('bgexpanded').slideToggle('fast');
			}
		});

		// general expand/collapse
		bodyObj.on('click keypress','a[data-behavior="expandCollapse"]',function(event) {
		  var thisA;
		  if(event.type !== 'keypress' || event.which === 13) // allow tabbing
			{
			event.preventDefault();
			thisA = $(this);
			if(thisA.hasClass('collapsed') || thisA.hasClass('expanded'))
			  { thisA.toggleClass('collapsed expanded'); }
			if(thisA.hasClass('expanded')){ thisA.find('span.icon').html('Hide '); }
			else { thisA.find('span.icon').html('Show '); }
			toggleAttribute(thisA,'aria-expanded');
			$(thisA.attr('href')).slideToggle('fast');
			}
		});

		$('ul.viewSwitch li button').on('click keypress',function(event) {
		  if(event.type !== 'keypress' || event.which === 13)
			{
			var thisClick = $(this);
			if(thisClick.hasClass('selected')) { return false; }
			thisClick.closest('ul').find('button.selected').removeClass('selected');
			thisClick.addClass('selected');
			}
		});

		// open pdf
		$('a.pdfLink,a.pdfOpen,a.pdfDownload').on('click keypress',function(event) {
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			var thisClick = $(this),url;
			thisClick.blur();
			url = thisClick.attr('href');
			if(url === '' || url === '#')
			  { url = '../assets/pdf/sample.pdf'; } // default
			window.open(url,'','width=986');
			}
		});

		// open in new window
		$('a.newWindow,a[data-prototype~="newWindow"]').on('click keypress',function(event) {
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			var thisClick = $(this),url;
			thisClick.blur();
			url = thisClick.attr('href');
			if(thisClick.attr('target') !== '_blank')
			  {
			  window.open(url,'_blank','left=10,menubar=yes,resizable=yes,scrollbars=yes,status=yes,width=1161');
			  }
			}
		});
		// end page links

		// toggle visibility
		$('a[data-prototype="toggleContent"]').on('click keypress',function(event) {
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			var thisObj = $(this);
			thisObj.blur();
			thisObj.find('span').toggleClass('hidden');
			$(thisObj.attr('href')).slideToggle();
			}
		});

		// help popup: create dialog, create listeners
		if($('#helpPopup').length > 0)
		{
		helpWidth = 380;
		helpPopup = $('#helpPopup');

		$('a.clickHelp,a.clickHelpLink').on('click keypress',function(){
		  var leftPos,offset,thisClick,thisClickWidth,thisHeight,thisNext,thisPos,topPos;
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			thisClick = $(this);
			thisClick.blur();
			if(thisClick.parents().hasClass('disabled')) { return; }

			$('div.helpContent').addClass('hidden'); // reset messages
			currentPopupObj = $(thisClick.attr('href'));
			helpPopup.dialog('option','title','Help');
			currentPopupObj.removeClass('hidden'); // show selected message

			// determine position
			thisPos = 0;
			thisClickWidth = 0;
			if(thisClick.hasClass('clickHelpLink') && thisClick.next().hasClass('clickHelp'))
			  {
			  thisNext = thisClick.next();
			  thisPos = thisNext.position();
			  offset = thisNext.viewportOffset();
			  }
			else
			  {
			  thisPos = thisClick.position();
			  thisClickWidth = thisClick.width();
			  offset = thisClick.viewportOffset();
			  }

			helpPopup.dialog('open');

			thisHeight = helpPopup.parent().height(); // after open

			// position popup
			containerMidpoint = $('#container').width()/2;
			if(thisPos.left > containerMidpoint) // open to left of ? icon
			  {
			  leftPos = offset.left-helpWidth;
			  topPos = offset.top - thisHeight - 10;
			  }
			else // open to right, centered vertically
			  {
			  leftPos = offset.left+20+thisClickWidth;
			  topPos = offset.top - ((thisHeight - 12)/2); // 12 = background icon height
			  }

			helpPopup.dialog('option','position',[leftPos,topPos]);
			}
		});

		} // end help popup


		// Messages flyout for comm center, very down and dirty.
		// TODO:  Needs the ability to focus into the flyout, etc. etc. etc.

	if ($('#messagesFlyout').length > 0){

		var messagesFlyoutMenu = function(e){
				var messagesLink = $('#messagesFlyoutLink')
				, messagesFlyout = $('#messagesFlyout')
				//, messageCenterBtn = messagesFlyout.find('a.btn')
				, flyoutLinks = messagesFlyout.find('a')
				, flyoutHovered = false
				, flyoutLinkHovered = false
				, messagesLinkHovered = false
				;

			messagesLink.on('mouseenter focus',function(e) {

				var pos = $(this).position()
				,	width = $(this).outerWidth()
				;

			 	messagesFlyout.css({
				        position: "absolute",
				    // 	top: (pos.top)+ 18 + "px",
				        left: -(pos.left + width) + "px"
				    }).stop().show();

				messagesLinkHovered = true;

			});


			messagesLink.on('mouseleave blur',function(e) {
					messagesLinkHovered = false;

			 		var flyoutTimeout = setTimeout(function(){
			 			if ((!messagesLinkHovered) && (!flyoutHovered)){
			 				messagesFlyout.stop().fadeOut();
			 			}

			 		}, 250);

			});


			messagesFlyout.on('mouseenter',function(e) {
				flyoutHovered = true;
				messagesLinkHovered = false;
			});

			messagesFlyout.on('mouseleave',function(e) {
				flyoutHovered = false;
				messagesLinkHovered = false;

				var flyoutTimeout = setTimeout(function(){
		 			if (!flyoutHovered){
		 				messagesFlyout.stop().fadeOut();
		 			}

		 		}, 250);
			});



			 flyoutLinks.filter('a:not(.popup-close):first').on('focus',function(e) {
				flyoutHovered = true;
			 	flyoutLinkHovered = true;
			 });

			flyoutLinks.filter('a:last').on('blur',function(e) {
				flyoutHovered = false;
				flyoutLinkHovered = false;


				var flyoutTimeout = setTimeout(function(){
		 			if (!flyoutLinkHovered){
		 				messagesFlyout.stop().fadeOut();
		 			}

		 		}, 250);
			});





			messagesFlyout.find('.popup-close').on('click', function(e){
				e.preventDefault();
				messagesFlyout.stop().fadeOut();
				flyoutHovered = false;
				messagesLinkHovered = false;
			});

		}

		messagesFlyoutMenu();

	}






		// 2/27/14 - WHY IS THERE ANOTHER (HORRIFIC) COPY OF THE TOOLTIP COMPONENT CODE IN THIS FILE!?! - JW

		// tip hover popup (supports tablet UI)
		if($('a.tipLink,a.tipLink2,a.infoLink,input.infoLink,input.tipLink,input.tipLink2').length > 0)
		  {
		  bodyObj.on('click','#tooltip',function(event){
			event.preventDefault();
			$('#tooltip').remove(); // tablet clickable close
		  });

		bodyObj.on('blur','input.infoLink,input.tipLink,input.tipLink2',function(event){
		  event.preventDefault();
		  $('#tooltip').remove(); // tablet clickable close
		});

		tooltip = function(){

		bodyObj.on('click mouseenter focus','a.infoLink,a.tipLink,a.tipLink2,input.infoLink,input.tipLink,input.tipLink2',function(event){
		  event.preventDefault();
		  var containerMidpoint,dataTipLocation,dataTipWidth,tipLinkLeftPos,leftRight,pointerObj,pointerHeight,pointerStop,t,tipLink = $(this),timeoutVal,tipLink2,tipLinkHeight,tipLinkPos,tipLinkTitle,tipLinkWidth,tooltip,tooltipClass,tooltipHeight,tooltipHeightHalf,tooltipTop,tooltipWidth,topOffset,topPos,windowHeight,windowObj;
		  if(tipLink.css('visibility') === 'hidden') { return; }

		  $('#tooltip').stop().remove();
		  if($(tipLink.attr('href')).length > 0)
			{
			// create separate tag and assign anchor's href attribute as the id
			tipLinkTitle = $(tipLink.attr('href')).html(); // href="#text" -> id="text"
			}
		  else if($('#'+tipLink.attr('aria-describedby')).length > 0)
			{
			// create separate tag and assign anchor's href attribute as the id
			tipLinkTitle = $('#'+tipLink.attr('aria-describedby')).html(); // aria-describedby="#text" -> id="text"
			}
		  else
			{
			tipLinkTitle = tipLink.attr('title'); // set title in tipLink anchor
			}
		  if(tipLinkTitle === '' || tipLinkTitle === undefined)
			{
			tipLinkTitle = tipLink.data('title');
			}
		  if(tipLinkTitle === undefined) // final check
			{
			tipLinkTitle = 'See copydeck for help text.';
			}
		  else // store data since preventing default tooltip clears source
			{ tipLink.data('title',tipLinkTitle); }
		  tipLink.attr('title',''); // prevent default tooltip
		  bodyObj.append('<div id="tooltip"><a class="popup-close" href="#closeTip"><span class="popup-close-icon">Close</span></a><div class="pointer"></div><div class="bd">'+ tipLinkTitle +'</div></div>');

		  tooltip = $('#tooltip');

		  // position popup after open
		  containerMidpoint = $('#container').width()/2; // for left/right tooltips
		  tipLink2 = tipLink.hasClass('tipLink2'); // test for alternate source

		  tipLinkPos = tipLink.offset();
		  tipLinkLeftPos = tipLinkPos.left;
		  leftRight = 'left';
		  tipLinkHeight = tipLink.height();
		  if(tipLinkLeftPos > containerMidpoint) // pointer heights differ; store before tipLinkLeftPos adjustments
			{
			pointerHeight = 32; // 25
			pointerStop = -5;
			}
		  else
			{
			pointerHeight = 20; // 19
			pointerStop = 1;
			}

		  // display the tooltip
		  tooltip
				 .stop()
				 .css({'left':'-99999em','zIndex':getzIndex()+10,'maxWidth':containerMidpoint+'px'})
				 .fadeIn();

		  tooltipWidth = Math.min(530,tooltip.width()); // fit in window horizontally
		  tipLinkWidth = tipLink.width();

		  dataTipWidth = Number(tipLink.attr('data-tooltip-width')); // markup override
		  if(dataTipWidth !== undefined && dataTipWidth > 0)
			{ tooltipWidth = dataTipWidth; }
		  tooltip.css('width',tooltipWidth + 'px'); // set width first to establish tooltip height

		  dataTipLocation = tipLink.attr('data-tooltip-location'); // markup override
		  if(dataTipLocation === undefined) { dataTipLocation = ''; }
		  tooltipClass = 'infoHover visible ';
		  pointerObj = tooltip.find('div.pointer');
		  switch(dataTipLocation)
			{
			case 'bottom':
			  tipLinkLeftPos = tipLink2 ? tipLinkLeftPos - tooltipWidth/2 + tipLinkWidth/2 : tipLinkLeftPos - tooltipWidth/2 + tipLinkWidth*1.5 - 11;
			  tooltipClass = tooltipClass + 'pointerTop';
			  topOffset = -(tipLinkHeight + 5);
			  pointerObj.css('right',tooltipWidth/2 - 9);
			  break;
			case 'left':
			  tipLinkLeftPos = tipLinkLeftPos - tooltipWidth - 10;
			  tooltipClass = tooltipClass + 'pointerRight';
			  topOffset = tooltip.height()/2 - tipLinkHeight/2;
			  break;
			case 'top':
			  tipLinkLeftPos = tipLink2 ? tipLinkLeftPos - tooltipWidth/2 + tipLinkWidth/2 : tipLinkLeftPos - tooltipWidth/2 + tipLinkWidth*1.5 - 11;
			  tooltipClass = tooltipClass + 'pointerBottom';
			  topOffset = tooltip.height() + tipLinkHeight;
			  pointerObj.css('right',tooltipWidth/2 - 9);
			  break;
			default:
			  if(tipLinkLeftPos > containerMidpoint)
				{
				tipLinkLeftPos = tipLinkLeftPos - tooltipWidth - 10;
				tooltipClass = tooltipClass + 'pointerRight';
				}
			  else
				{
				tipLinkLeftPos = tipLink2 ? tipLinkLeftPos + tipLinkWidth : tipLinkLeftPos + tipLinkWidth;
				tooltipClass = tooltipClass + 'pointerLeft';
				}
			  topOffset = tooltip.height()/2 - tipLinkHeight/2;
			  break;
			} // end switch location

		  topPos = tipLinkPos.top - topOffset;

		  if(topPos > windowHeight) // when tabbing to tooltips, if tipLink is off-screen
			{ timeoutVal = 3000; } // pause; page scrolls first, then tooltip vertically positions correctly
		  else
			{ timeoutVal = 0; }

		  t = setTimeout(function(){

		  // ensure tooltip fits in window vertically, adjust pointer
		  windowObj = $(window);
		  windowHeight = windowObj.height();
		  tooltipTop = topPos - windowObj.scrollTop();
		  tooltipHeight = tooltip.outerHeight();
		  tooltipHeightHalf = tooltipHeight/2;

		  if((tooltipTop + tooltipHeight) > windowHeight) // tooltip crosses page bottom
			{
			if(dataTipLocation === 'bottom') // won't fit, reverse position
			  {
			  tooltipClass = tooltipClass.replace(/pointerTop/,'pointerBottom');
			  topOffset = tooltipHeight + tipLinkHeight;
			  topPos = tipLinkPos.top - topOffset;
			  }
			else
			  {
			  topPos = topPos - (tooltipTop + tooltipHeight - windowHeight);
			  tooltip.find('.pointer').css({'top':'auto','bottom':Math.max(windowHeight - tooltipTop - tooltipHeightHalf - pointerHeight/2,pointerStop)});
			  }
			}
		  else if(tooltipTop < 0) // tooltip crosses page top
			{
			if(dataTipLocation === 'top') // won't fit, reverse position
			  {
			  tooltipClass = tooltipClass.replace(/pointerBottom/,'pointerTop');
			  topOffset = tipLinkHeight + 5;
			  topPos = tipLinkPos.top + topOffset;
			  }
			else
			  {
			  topPos = topPos - tooltipHeightHalf + (tooltipHeightHalf - tooltipTop);
			  pointerObj.css('top',Math.max(tooltipTop + tooltipHeightHalf,9));
			  }
			}


		  // position the tooltip
		  tooltip.addClass(tooltipClass);
		  tooltip.css('top',topPos + 'px')
				 .css(leftRight,tipLinkLeftPos + 'px');

		  },timeoutVal);

		  });

		bodyObj.on('mouseleave','a.tipLink,a.tipLink2,a.infoLink,input.infoLink,input.tipLink,input.tipLink2',function(){
		  var tId;
		  // remove tip when not locked (hovered, allows links inside tip)
		  tId = window.setTimeout(function(){ // allow time to hover tip
			if(!$('#tooltip').hasClass('tipLock')) { $('#tooltip').fadeOut(function(){$(this).remove();}); }
			}, 100);
		  });

		bodyObj.on('mouseenter','#tooltip',function(){
		  $('#tooltip').addClass('tipLock'); // lock tip in place while hovering
		  }).on('mouseleave','#tooltip',function(){
			$('#tooltip').removeClass('tipLock').fadeOut(function(){$(this).remove();});
		  }); // close mouseleave

		}; // close tooltip()

		tooltip(); // invoke tooltip

		bodyObj.on('click','a.tipLink,a.tipLink2,a.infoLink,input.infoLink,input.tipLink,input.tipLink2',function(event){
		  event.preventDefault();
		  $(this).blur();
		});
		} // end tip

		// ++++++++++++++++++++++++
		if($('#cancelPopup').length > 0)
		{
		cancelPopup = $('#popupCancel');

		$('a.clickCancel').on('click keypress',function(event){
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			var thisClick = $(this);
			thisClick.blur();

			$('div.cancelContent').addClass('hidden'); // reset messages
			$(thisClick.attr('href')).removeClass('hidden'); // show selected message

			cancelPopup.dialog('open');
			}

		});
		} // end cancelPopup

		$('a.closePopup,a[href="#closePopup"],input[data-behavior~="closePopup"],button[data-behavior~="closePopup"]').on('click keypress',function(event){
		  if(event.type !== 'keypress' || event.which === 13)
			{
			if($(this).is('a')) { event.preventDefault(); } // prevent anchor behavior
			var popupObj = $(this).closest('div[class~=popup]');
			if(popupObj.attr('id') === undefined)
			  { popupObj = $(this).closest('form[class~=popup]'); }
			popupObj.dialog('close');
			}
		});

		// exitWithoutSaving popup: create dialog, use generic close listener
		if($('#exitWithoutSavingPopup').length > 0)
		{

		exitWithoutSavingPopup = $('#exitWithoutSavingPopup');

		$('a.glossary,#pageUtilLinks a.glossaryLink').off('click keypress');

		$('a.glossary,#pageUtilLinks a.glossaryLink').on('click keypress',function(event){
		  var tId;
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			$('#exitWithoutSavingPopup div.exitMessage').removeClass('hidden');
			$('#exitWithoutSavingPopup div.saveMessage').addClass('hidden');
			popupTitle = $('#ui-dialog-title-exitWithoutSavingPopup');
			popupTitle.removeClass('hidden');
			popupTitle.next('a').removeClass('hidden');

			$('#exitWithoutSavingPopup a').on('click keypress',function(event){
			  var popupClick = $(this);
			  if(popupClick.hasClass('exitSave'))
				{
				if(event.type !== 'keypress' || event.which === 13)
				  {
				  event.preventDefault();
				  $('#exitWithoutSavingPopup div.exitMessage').addClass('hidden');
				  $('#exitWithoutSavingPopup div.saveMessage').removeClass('hidden');
				  popupTitle = $('#ui-dialog-title-exitWithoutSavingPopup');
				  popupTitle.addClass('hidden');
				  popupTitle.next('a').addClass('hidden');
				  tId = window.setTimeout(function(){window.location.href='placeholder.html?pht=Glossary';exitWithoutSavingPopup.dialog('close');},1200);
				  }
				}
			  else
				{
				exitWithoutSavingPopup.dialog('close');
				}
			  });
			exitWithoutSavingPopup.dialog('open');
			}
		});

		} // end exitWithoutSavingPopup behavior

		// popup: create dialog, use generic close listener
		if($('#popupLeaving').length > 0)
		{

		popupLeaving = $('#popupLeaving');

		$('[data-prototype~="openProviderAccount"] a.newWindow,[data-prototype~="openProviderAccount"] a[data-prototype~="newWindow"],[data-behavior="popupLeaving"]').off('click keypress'); // disable default behavior
		$('[data-prototype~="openProviderAccount"] a.newWindow,[data-prototype~="openProviderAccount"] a[data-prototype~="newWindow"],[data-behavior="popupLeaving"]').on('click keypress',function(event) {
		  var thisClick;

		  thisClick = $(this);
		  thisClick.blur();

		  if(thisClick.attr('data-prototype'))
			{
			if(thisClick.attr('data-prototype').indexOf('tiaaCref') !== -1)
			  { return; } // not leaving site
			}
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			bodyObj.data('hrefProviderAccount',thisClick.attr('href'));
			popupLeaving.dialog('open');
			}
		});

		popupLeaving.find('input[data-behavior="continueLeaving"]').on('click keypress',function(event) {
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			popupLeaving.dialog('close');
			window.open(bodyObj.data('hrefProviderAccount'),'_blank','left=10,menubar=yes,resizable=yes,scrollbars=yes,status=yes,width=1161');
			}
		});

		} // end popupLeaving behavior

		if($('#popupSaveReturn').length > 0)
		{
		// popup: create dialog, create listeners
		popupSaveReturn = $('#popupSaveReturn');
		popupSaveReturn.find('a[href="#discard"]').on('click keypress',function(event) {
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			window.location.href = "../../integrated/authentication/logout.html";
			}
		});
		popupSaveReturn.find('a[href="#saveExit"]').on('click keypress',function(event) {
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			window.location.href = "../../integrated/authentication/logout.html?page=saved";
			}
		});

		$('a.saveLink,input[data-behavior="saveReturn"]').on('click keypress',function(event) {
		  var thisObj = $(this);
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			thisObj.blur();
			if(thisObj.hasClass('disabled')) { return; }
			popupSaveReturn.dialog('open');
			}
		});
		} // end popup


		if($('#popupFundProfile').length > 0)
		{
		// popup: create dialog, create listeners
		popupFundProfile = $('#popupFundProfile');

		$('a[href="#fundProfile"]').on('click keypress',function(event) {
		  var thisObj = $(this);
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			thisObj.blur();
			if(thisObj.hasClass('disabled')) { return; }
			popupFundProfile.dialog('open');
			}
		});
		} // end popup

		// simulate processing
		if($('#popupWait').length > 0)
		{
		popupWait = $('#popupWait')
		  .dialog({
				  autoOpen:false,
				  close:function(){
					$(this).data('openerObj').focus(); // return focus to opening button
					},
				  closeOnEscape:true,
				  dialogClass:'modal', // standard class
				  draggable:false,
				  minHeight:20,
				  modal:true,
				  open:function(){
					$('#ui-dialog-title-popupWait').next('a.ui-dialog-titlebar-close').addClass('hidden');
					configurePopup();
					},
				  resizable:false,
				  width:380
				  });

		$('a.popupWait').on('click keypress',function(event){
		  var thisClick,tId;
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			thisClick = $(this);
			thisClick.blur();
			timeoutValue = getNumber(thisClick.attr('href'),'numeric'); // optional customizable value, in ms
			if(timeoutValue === '') { timeoutValue = 1000; } // 1 second, quick for prototype

			popupWait.dialog('open');
			tId = window.setTimeout(function(){$('#popupWait').dialog('close');},timeoutValue);
			}
		});
		} // end popupWait

		// index functions
		toggleShortcuts = $('#toggleShortcuts');
		if(toggleShortcuts.length > 0)
		  {
		  toggleShortcuts.html('<span class="hidden">[+] Show</span><span>[-] Hide</span> Shortcuts'); // initialize
		  if($('ul.shortcuts').hasClass('closed') || $('li.shortcuts').hasClass('closed'))
			{
			toggleShortcuts.find('span').toggleClass('hidden');
			}
		  toggleShortcuts.on('click keypress',function(event){
		  if(event.type !== 'keypress' || event.which === 13)
			{
			event.preventDefault();
			toggleShortcuts.blur();
			toggleShortcuts.find('span').toggleClass('hidden');
			$('ul.shortcuts,li.shortcuts').slideToggle();
			}
		  });
		  } // end

		$('#logo a').on('click', function(event) {
			event.preventDefault();
			window.location = 'homepage.html';
		});

		$('a.glossary,#pageUtilLinks a.glossaryLink').on('click', function(event) {
			event.preventDefault();
			window.location = '/releaseb/help/glossary.asp';
		});


		$('a.printLink:not(.jq-actions),a.printBtn:not(.jq-actions)').on('click',function(e) {
				e.preventDefault();
				$(this).blur();
				window.print();
		});

		$('a.printLink').attr('title','Print this page.');

		$('#printMenu a, #printMenu-sections a').on('click',function(e){
				e.preventDefault();

				var $link = $(this)
				,	isGtIe8 = ($.support.opacity)
				,	sectionId = $link.get(0).href.split('#')[1]
			, 	cssFiles = document.styleSheets
				, 	styleRules
			, 	styleItemRule
				,	printSection
			,	winPrint
			;

			printSection = function(e){
				winPrint = window.open('', '', 'left=0,top=0,width=800,height=600,toolbar=0,scrollbars=1,status=0');
				winPrint.document.write('<!DOCTYPE HTML><html class="print">\n<head>\n');
				winPrint.document.write('<title>Print Page</title>\n');

				$.each( cssFiles, function(cssFileKey,cssFileVal) {

					styleRules = (isGtIe8) ? cssFileVal.cssRules : cssFileVal.rules;

					// if there's an inline style block in the <head> then add that too
					if ((cssFileVal.href == null && styleRules.length > 0) || (cssFileVal.href == '' && styleRules.length > 0)) {
						winPrint.document.write('<style>\n');

						$.each(styleRules, function(cssKey,cssVal) {
							styleItemRule = (isGtIe8) ? cssVal.cssText : cssVal.selectorText+'{'+cssVal.style.cssText+'}';
							winPrint.document.write(styleItemRule);
						});

						winPrint.document.write('</style>\n');
					} else {
						winPrint.document.write('<link href="'+cssFileVal.href+'" rel="stylesheet" type="text/css" />\n');
					}

				});

				winPrint.document.write('</head>\n');
				winPrint.document.write('<body>\n');
				winPrint.document.write($("<div />").append($('#'+sectionId).clone()).html());
				winPrint.document.write('</body>\n');
				winPrint.document.write('</html>\n');
				winPrint.document.close();
				winPrint.focus();
				winPrint.print();
				winPrint.close();
			}

			// backwards compatibility for print links still using inline JS in the HREF to print the page
			if (sectionId == 'whole-page' || sectionId == 'javascript:window.print();'){
				window.print();

			// check if the link has an existing inline onclick event. If it does, let it fire!
			} else if ($link.attr('onclick')) {
				$link.trigger('click');
			} else {
				printSection(sectionId);
			}


			});

		$('a.disabled').on('click', function(event) {
			event.preventDefault();
			$(this).blur();
		});

		$('a.pdfLink,a.pdfOpen,a.pdfDownload').on('click', function(event) {
			event.preventDefault();
			var thisClick = $(this);
			thisClick.blur();
			var url = thisClick.attr('href');
			if(url == '' || url == '#') {
				url = 'assets/pdf/sample.pdf';
			} // default
			window.open(url,'','width=986');
		});

		$('a.newWindow').on('click', function(event) {
			event.preventDefault();
			var thisClick = $(this);
			thisClick.blur();
			var url = thisClick.attr('href');
			window.open(url,'','width=986');
		});


	})();

	(function($) {
		'$:nomunge'; // Used by YUI compressor.

		var win = $(window);

		$.fn.viewportOffset = function() {
			var offset = $(this).offset();

			return {
				left: offset.left - win.scrollLeft(),
				top: offset.top - win.scrollTop()
			};
		};
	})(jQuery);

	/* placeholder title; requires placeholder.htm */
	// sample: <a href="placeholder.html?pht=Learn+More">Learn more.</a>

	if($('body').hasClass('placeholder')) {
		var pht = qp('pht') ? unescape(qp('pht').replace(/\+/g,' ')) : '';
		if(pht != '') {
			iH('pht',pht);
			document.title = 'TIAA Cref | ' + pht;
		}
	}

	// Handle Super User Mode
	if($('.superUser').find('a,input[type=text],input[type=button]').length){
		$('.superUser:not(nav)').find('a:not(.live)').addClass('anchorOff');
		$('.superUser:not(nav)').find('input[type=button]').addClass('btnOff');
		$('.superUser:not(nav)').find('input[type=text]').prop('disabled',true);

		// $('nav.superUser a#menu-AdvicePlanning, nav.superUser a#menu-ResearchPerformance, nav.superUser #menu-ProductServices').addClass('anchorOff').attr('href','javascript:void(0)').on('click',function(e){
		// 	e.preventDefault();
		// 	e.stopImmediatePropagation();
		// });

		// $('nav.superUser #menu-AccountHome').next('div').find('a').last().addClass('anchorOff');
		// $('nav.superUser #menu-ManagePortfolio, nav.superUser #menu-AdvicePlanning, nav.superUser #menu-ManagePortfolio, nav.superUser #menu-ProductServices').next('div').find('a').addClass('anchorOff');
		// $('nav.superUser #menu-ManagePortfolio').next('div').find('.mm_col:eq(1) a:eq(7)').removeClass('anchorOff').attr('href','http://prototype.ops.tiaa-cref.org/pt/lumpsums/withdrawselection/v_0.4/gettingstarted.html?myTCSimulation');
		// $('nav.superUser #menu-ManagePortfolio').next('div').find('.mm_col:eq(1) a:eq(8)').removeClass('anchorOff').attr('href','http://prototype.ops.tiaa-cref.org/pt/lumpsums/withdrawselection/v_0.4/managewithdrawals.html?myTCSimulation');
		// $('nav.superUser #menu-ResearchPerformance').next('div').find('a').not(':first').addClass('anchorOff');

		// $('nav.superUser #menu-AdvicePlanning').next('div').find('.mm_row').first().find('.mm_col:eq(4)').hide();
		// $('nav.superUser #menu-AdvicePlanning').next('div').find('.mm_row').last().find('button').addClass('btnOff');
	}

	if($('body.simulation').length){
		$('.closeSimulation').on('click',function(e){
			e.preventDefault();
			window.open('','_self','');
        	window.close();
		});
	}

	if($('body').hasClass('simulation')) {
		//$('a.btnOff').toggleClass('btnOff simulationBtn')
	}


	/* FUND HOVERS
		//TODO: Add way to componentize this functionality
	*/

	// Handle Fund Hover Sliders
	if($('#slider-amount-range').length){
		var $slider = $('#slider-amount-range').slider({
			range:true,
			min:0,
			max:100,
			step: 0.01,
			values:[ 19.29, 24.45 ],
			slide: function( event, ui ) {
				$( ".slider-amount" ).text( "$" + ui.values[ 0 ].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "-$" + ui.values[ 1 ].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
			}
		});
		$( ".slider-amount" ).text( "$" + $( "#slider-amount-range" ).slider( "values", 0 ).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "-$" + $( "#slider-amount-range" ).slider( "values", 1 ).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );

		function addSlider(){
			setTimeout(initSlider, 250);
		}
		function doNothing(){

		}
		$('.equity-hover').hoverIntent({
	        over: addSlider,
	        out: doNothing,
	        interval: 250
		});


		function initSlider(){
			$("#tooltip .slider").slider({
				range:true,
				min:0,
				max:100,
				step: 0.01,
				values:[ 19.29, 24.45 ],
				slide: function( event, ui ) {
					$( "#tooltip .slider-amount" ).text( "$" + ui.values[ 0 ].toFixed(2) + "-$" + ui.values[ 1 ].toFixed(2) );
				}
			});
			$( "#tooltip .slider-amount" ).text( "$" + $( "#tooltip .slider" ).slider( "values", 0 ).toFixed(2) + "-$" + $( "#tooltip .slider" ).slider( "values", 1 ).toFixed(2) );
		}
	}

	//Handle Fund Hover Buttons
	$(document.body).on('click','#tooltip .btnBar a',function(e){
		e.preventDefault()
		if($(this).attr('href') != '#'){
			window.location.href = $(this).attr('href');
		}
	});


} // end initialization

jQuery(document).bind('dc-after-load', function() {
	$('body').addClass('content-loaded');
});

jQuery(document).ready( function($) {
	initPrototype();
});

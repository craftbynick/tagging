(function(){
	window.TIAAPROTO = window.TIAAPROTO || {};
	TIAAPROTO.env = TIAAPROTO.env || {};
	TIAAPROTO.env.root = '/tiaa-cref.org/';
	TIAAPROTO.env.experience = 'default';
	TIAAPROTO.env.experience_obj = {"release":{"is":{"release_2015-05":true}},"retirement_prr_type":{"is":{"percent":true}},"statements_file":{"is":{"release3":true}},"brokerage_bricklet":{"is":{"new":true}},"advisor":{"is":{"assigned":true}},"advisor_img":{"is":{"true":true}},"megamenu":{"is":{"silver":true}},"show_outside_assets":{"is":{"true":true}},"multifactor":{"is":{"false":true}},"accepted_terms":{"is":{"true":true}},"brokerage_rtq_acknowledged":{"is":{"false":true}},"transactionalert":{"is":{"true":true}},"has_bank_data":{"is":{"true":true}},"bank_data_status":{"is":{"available":true}},"prr":{"is":{"true":true}},"mytc4":{"is":{"true":true}},"mytc-thin-brick":{"is":{"true":true}},"portfolio_summary_restricted_list":{"is":{"true":true}},"art_destination_funds":{"is":{"expanded":true}},"retirement_plan_line":{"is":{"name then description":true}},"concept-prr-v2":{"is":{"true":true}}};
	TIAAPROTO.env.institution_obj = {"id":"1","name":"TIAA-CREF","page_title_prefix":"TIAA-CREF","long_name":"TIAA CREF - Financial Services","logo":"assets\/images\/logo_tiaa.png","logo_alt":"TIAA CREF - Financial Services","plan_prefix":null,"address_id":"7"};
	TIAAPROTO.data = TIAAPROTO.data || {};
})();

	TIAAPROTO.utils = TIAAPROTO.utils || {};
	TIAAPROTO.utils.advisor = TIAAPROTO.utils.advisor || {};


	TIAAPROTO.utils.advisor.advisor_email = function() {
		var $modal = $('#modal-email-advisor-confirm')
			, $submitBtn = $modal.find('a[href="#submit"]')
			;


			$submitBtn.click(function(e){
				e.preventDefault();
				var $link = $modal.data('openerObj')
				,	$emailAddy = $link.data('mailto')
				,	emailAdvisorTimeout
				; 
				
				window.location.href = 'mailto:'+$emailAddy+'';
				$modal.dialog('close');
				
			});
	}

	TIAAPROTO.utils.advisor.advisor_request_callback = function() {

			var $modal = $('#modal-schedule-call-advisor')
			, 	$opener = $modal.data('openerObj')
			, 	$form = $modal.find('#request_callback_form')
			,	$submitBtn = $modal.find('a[href="#submit"]')
			,	$cancelBtn = $modal.find('a[href="#cancel"]')
			,	$closeBtn = $modal.find('a[href="#close"]')
			//, 	$phoneTypeSelector = $modal.find('#phone_number_type')
			, 	$phoneExtensionContent = $modal.find('#business_phone_ext')
			, 	$callbackFormContent = $modal.find('#request-callback-form')
			,	$callbackConfirmContent = $modal.find('#request-callback-confirm')
			,	$alternativeNumber = $('#alternative_phone_number')
			,	$contactChoice = $('input[name="contact_choice"]')
			,	$numberOnFile = $contactChoice.filter(':checked').next('input[id^="onfile"]').val()
			, 	validateDate
			,	isValid = false
			, 	$errorModule = $callbackFormContent.find('.alertModule')
			,	$alertText = $form.find('.alertText')
			;

			 // set options for callback day datepicker 
			 $('#request_callback_day').datepicker({minDate: +1, beforeShowDay: $.datepicker.noWeekends});

			 // change title of dialog and request form content to advisor the user clicked on
			 $modal.on('dc-load-modal', function(e){
			 	var modalOpener = $modal.data('openerObj')
			 	,	advisorNameEl = modalOpener.closest('.flcontainer').find('p.nmb strong')
			 	, 	advisorWidget = modalOpener.closest('.calloutBlue')
			 	, 	advisorName = (advisorNameEl.length > 0) ? advisorNameEl.text() :  (advisorWidget.length > 0) ? advisorWidget.find('h3 + p > a').text() : $('#advisor-bio h1 span[data-advisorName]').text();
			 	;
 			 	$modal.dialog('option', 'title','Request a Callback from '+advisorName);
			 	$modal.find('span[data-advisorName]').text(advisorName);

			});

				
			$contactChoice.on('change',function(){
				
				$numberOnFile = $(this).filter(':checked').next('input[id^="onfile"]').val();

			});


			var selectedPhoneNumber = function(){
					var phoneNumber
					, contact_choice = $contactChoice.filter(':checked').val()
					, phoneNumberExt
					;

					switch (contact_choice){
						case 'onfile':
							phoneNumber = $numberOnFile;
						break;
						case 'alternative_phone_number':
							phoneNumberExt = ($('#phone_extension').val() !== '') ? ' Ext. '+$('#phone_extension').val() : '';
							phoneNumber = $alternativeNumber.val() + phoneNumberExt;
						break;
				}

				return phoneNumber; 
			}


			$alternativeNumber.focus(function(e){
				$('#alternative_phone_number_radio').prop('checked','checked');
			});



			validateDate = function() {
				// basic reg check for date format dd/mm/yyyy (technically allows dd.mm.yyyy but our datepicker does not)
     			var reg = new RegExp(/^(\d{2})([.\/-])(\d{2})\2(\d{4})$/),
     				val = $('#request_callback_day').val();

			     if (reg.test(val)) {
			     	isValid = true; 
			     } else {
			     	isValid = false;
			     }
 			}


			$submitBtn.click(function(e){
				e.preventDefault();
			
				validateDate();

				if (isValid){
					$errorModule.add($alertText).removeClass('hidden visible').addClass('hidden');
					$callbackFormContent.find('.lblFieldPair').removeClass('alertHighlight');


					$callbackFormContent.hide();
					$callbackConfirmContent.show();

					var topicListLen = $('#request_callback_topic option:selected').length;
					$('#callback-request-topic span').empty();


					$('#callback-request-number span').text(selectedPhoneNumber());
					$('#callback-request-day span').text($('#request_callback_day').val());
					$('#callback-request-time span').text($('#request_callback_time option:selected').text());

					$('#request_callback_topic option:selected').each(function(i, selected){ 
						$('#callback-request-topic span').append((topicListLen > 1 && i < topicListLen - 1) ? $(selected).text()+', ' : $(selected).text()); 
					});
			 	} else {
			 		$('#request_callback_day').parent().parent().addClass('alertHighlight');
					$errorModule.add($alertText).removeClass('hidden visible').addClass('visible');

			 	}
				

			});
		
			$modal.on('dc-close-modal', function(e){
				$callbackFormContent.show();
				$callbackConfirmContent.hide();
				$errorModule.add($alertText).removeClass('hidden visible').addClass('hidden');
				$callbackFormContent.find('.lblFieldPair').removeClass('alertHighlight');

			 });

			/*
			var initial_phone_type = function(){
				if ($phoneTypeSelector.val() == 'Business'){
					$phoneExtensionContent.attr('class','inline');
				} else {
					$phoneExtensionContent.attr('class','hidden');
				}
			}

			initial_phone_type();
			*/

	}

$(document).bind('dc-after-load', function() {
	TIAAPROTO.utils.advisor.advisor_request_callback();
	TIAAPROTO.utils.advisor.advisor_email();
});
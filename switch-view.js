/**
 * Attaches a named click event (btnSwitch) to buttons and anchors within the
 * switch classes (ynSwitch & viewSwitch) to toggle a 'selected' class on and off.
 *
 * -NOTE- The original function in TC_Widgets only works with anchors.
 */

;(function( $ ){

var $switchBtn = $('.ynSwitch li, .viewSwitch li');

$('.ynSwitch li, .viewSwitch li').each(function(i){
	$(this).find('a, button').attr('aria-selected', function(x, val ) {
		var isSelected;

		isSelected = ($(this).hasClass('selected')) ? true : false;
	 return isSelected;
	});
});

$(document).on('click.btnSwitch', '.ynSwitch li, .viewSwitch li', function(e){
    e.preventDefault();
    $(this)
    	.find('a, button')
    		.addClass('selected')
    		.attr('aria-selected','true')
    	.end()
    	.siblings()
    		.find('a, button')
    			.removeClass('selected')
    			.attr('aria-selected','false')
    			.trigger('swithTriggered');
});

})(jQuery);
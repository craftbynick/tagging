/**
 * Global MyTC Secure Methods - like sitewide popups
 * TIAA Prototyping
 * Last Updated: 6/13/2014 (update when this file is modified)
 * Authors: Calvin Lloyd <clloyd@tiaa-cref.org>
 * Victor Butler <vbutler@tiaa-cref.org>
 * Calvin Lloyd <clloyd@tiaa-cref.org>
 */

(function($){
	"use strict";
	$('#MOD_AlertHub').on( "dialogopen", function( event, ui ) {
		var opener = $(this).data('openerObj');
		var $this = $(this);
		$this.prev('.ui-dialog-titlebar').remove();
		$this.find('.closeLink').on('click', function(event){
			$this.dialog('close');
		});
		$this.find('#MOD_ActiveAlerts div.tabs-container ul.tabs li a').on('click', function(event){
			$this.find('#MOD_ActiveAlerts article[data-tabkey]').addClass('hidden').filter('[data-tabkey="'+$(this).data('tabkey')+'"]').removeClass('hidden');
		});
		$this.find('#MOD_HistoricalAlerts div.tabs-container ul.tabs li a').on('click', function(event){
			$this.find('#MOD_HistoricalAlerts article[data-tabkey]').addClass('hidden').filter('[data-tabkey="'+$(this).data('tabkey')+'"]').removeClass('hidden');
		});
		var switchers = $this.find('.HubSwitcher');
		switchers.on('click', function(event){
			var target = $(this).data('target');
			switchers.each(function(i, item){
				if (target == $(item).data('target')) {
					$('#'+$(item).data('target')).show();
				} else {
					$('#'+$(item).data('target')).hide();
				}
			});
		});
	} );

	// Disable Tooltips in Simulation Mode
	if($('span.tipOff').length){
		var tt = $('span.tipOff').find('a.infoLink,a.tipLink,a.tipLink2,input.infoLink,input.tipLink,input.tipLink2');
		tt.prop('disabled',true);
	}

	// Disable Buttons in Simulation Mode
	if($('body.simulation').length){
	    $('div[id^="messages-"][id$="-toolbar"]').children('a').addClass('simulationBtn');
	    $('#files-default-actions-toolbar').find('a').addClass('simulationBtn');

	    $(document).on('click', '.anchorOff, .simulationBtn', function(e){
			e.preventDefault()
		});
	}

})(jQuery);

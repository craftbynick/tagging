(function($){
	$('body').on('click', '.tipLock a', function(event){
		var href = $(this).attr('href'),
			id,elm;
		// Condition to check for MOD link
		if(!href.indexOf('fundSymbol=')){
			id = href.substring(href.indexOf('#')); // substring to get rid of IE full URL in href bug
			elm = $(id);
			if (elm.length && elm.data('popup')) {
				elm.dialog('open');
			}
		}
	});
	// Helper for modal links inside tooltips
	$(document).on('click', '#tooltip a', function(e){
		var url = $(this).attr('href');
		// check if first character is hash
		if(url.indexOf('#') == 0){
			// check if href points to a modal
			if($(url).data('popup') !== null){
				e.preventDefault();
				$(url).dialog('open');
			}
		}
	});
})(jQuery);
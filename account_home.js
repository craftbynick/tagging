(function() {
	"use strict";

	window.TIAAPROTO = window.TIAAPROTO || {};
	TIAAPROTO.data = TIAAPROTO.data || {};
	TIAAPROTO.utils = TIAAPROTO.utils || {};

	TIAAPROTO.utils.portfolio_summary = {};
	TIAAPROTO.utils.portfolio_summary.my_investments = function(chart_container) {
		if ($('[id="' + chart_container + '"]').length) {
			var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
				width = 688;
			if (chart_container.indexOf('large') > -1) {
				width = 828;
			} else if (typeof TIAAPROTO.env.experience_obj['mytc-thin-brick'] === 'object' && typeof TIAAPROTO.env.experience_obj['mytc-thin-brick'].is['true'] === 'boolean') {
				width = 450;
			}
			$('[id="' + chart_container + '"]').highcharts((chart_container.indexOf('large') > -1 ? 'StockChart' : 'Chart'), {

				chart: {
					// renderTo: 'details-chart-small',
					// zoomType: 'x',
					marginLeft: (typeof TIAAPROTO.env.experience_obj['mytc-thin-brick'] === 'object' && typeof TIAAPROTO.env.experience_obj['mytc-thin-brick'].is['true'] === 'boolean' ? 90 : 15),
					width: width,
					marginTop: 25,
					style: {
						fontFamily: 'Arial'
					}
				},

				legend: {
					enabled: false,
					layout: 'vertical',
					floating: true,
					verticalAlign: 'top',
					align: 'right'
				},

				xAxis: {
					type: 'datetime',
					// tickPixelInterval: 60,
					lineColor: '#cccccc', // match prod
					lineWidth: 0.3,
					dateTimeLabelFormats: {
						month: '%b \'%y',
						day: '%m/%d/%y'
					},
					labels: {
		                overflow: 'justify' // avoid overflowing of first x and y labels
		            },
					// startOnTick: false,
					// endOnTick: false,
					showFirstLabel: true, // added bcoz first label is shown in production
					showLastLabel: true,
					minPadding: (typeof TIAAPROTO.env.experience_obj['mytc-thin-brick'] === 'object' && typeof TIAAPROTO.env.experience_obj['mytc-thin-brick'].is['true'] === 'boolean' ? 0.04 : 0.09), // need to move the first tick (data point) so that tooltip won't cut off on 1024 resolution
					maxPadding: 0.02
					//maxZoom: 14 * 24 * 3600000
				},

				yAxis: {
					// max: 125,
					// tickInterval: 25,
					lineColor: '#cccccc', // match prod
					lineWidth: 0.3,
					gridLineColor: '#cccccc',
					gridLineWidth: 0.5,
					showLastLabel: true,
					opposite: (typeof TIAAPROTO.env.experience_obj['mytc-thin-brick'] === 'object' && typeof TIAAPROTO.env.experience_obj['mytc-thin-brick'].is['true'] === 'boolean' ? false : true),
					labels: {
						format: '${value}',
						formatter: function() { // added formatter to match production
							return '$' + Highcharts.numberFormat(this.value, 2);
						},
						align: (typeof TIAAPROTO.env.experience_obj['mytc-thin-brick'] === 'object' && typeof TIAAPROTO.env.experience_obj['mytc-thin-brick'].is['true'] === 'boolean' ? 'right' : 'left'), // fix overlapping label in view larger
						x: (typeof TIAAPROTO.env.experience_obj['mytc-thin-brick'] === 'object' && typeof TIAAPROTO.env.experience_obj['mytc-thin-brick'].is['true'] === 'boolean' ? -10 : 10),
						y: 3
					},
					title: {
						text: null
					}
				},

				title: {
					// remove for IE
					text: ''
				},

				credits: {
					enabled: false,
					text: null
				},

				navigator: {
			    	enabled: false
			    },

			    rangeSelector: {
			    	enabled: false
			    },

		        scrollbar: {
			    	enabled: false
			    },

				tooltip: {
					shared: true,
					valueDecimals: 2,
					valuePrefix: '$',
					crosshairs: false,
					formatter: function() {
						var date = new Date();
						date.setTime(this.x);
						var s = '<div class="infoHover pointerBottom" style="border:1px solid #d6d6d6;padding:10px;box-shadow:1px 2px 10px -1px #cccccc;background-color:#ffffff;"><div class="pointer" style="margin-top:-1px;"></div><b>' + monthNames[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear() + '</b>';

						var prr_data = [];

						for (var i = this.points.length - 1; i >= 0; i--) {
							var point = this.points[i];
							s += '<br /><span style="color: ' + point.series.color + ';">' + point.series.name + ': ' +
								accounting.formatMoney(point.y) + '</span>';
							prr_data.push(point.y);
						}

						s += '</div>';

						return s;
					},
					positioner: function(boxWidth, boxHeight, point) {
			            return {
			                x: point.plotX + (typeof TIAAPROTO.env.experience_obj['mytc-thin-brick'] === 'object' && typeof TIAAPROTO.env.experience_obj['mytc-thin-brick'].is['true'] === 'boolean' ? -36 : -113),
			                y: point.plotY + -66
			            };
			        },
					backgroundColor: 'transparent',
					borderColor: 'transparent',
					borderWidth: 0,
					borderRadius: 0,
					shadow: false,
					useHTML: true,
					/*
					borderRadius: 0,
					shadow: false,
					backgroundColor: {
						linearGradient: [0, 0, 0, 40],
						stops: [
							[0, 'rgba(151, 226, 248, 0.3)'],
							[1, 'rgba(255, 255, 255, 0.9)']
						]
					},
					*/
					style: {
						fontFamily: 'Arial',
						fontSize: '12px',
						padding: '10px',
						lineHeight: '20px',
						whiteSpace: 'wrap'
					}
				},

				series: [{
						type: 'area',
						lineWidth: 2,
						name: 'Retirement Contributions',
						color: 'rgb(0,180,232)',
						fillColor: {
							linearGradient: [0, 200, 0, 80],
							stops: [
								[0, 'rgba(255, 255, 255, 0.5)'],
								[1, 'rgba(4, 144, 184, 0.15)']
							]
						},
						marker: {
							enabled: false
						},
						threshold: null,
						pointInterval: 30 * 24 * 3600 * 1000, // one month
						data: TIAAPROTO.data.portfolio_summary.my_investments.investment
					}, {
						type: 'line',
						lineWidth: 2,
						name: $('[id="' + chart_container + '"]').closest('.portfolio-summary').find('span.legend-name').text(),
						marker: {
							enabled: false
						},
						color: 'rgb(141, 71, 143)',
						data: TIAAPROTO.data.portfolio_summary.my_investments.total_balance
					}
				]

			});
			$('[id="' + chart_container + '"]').data('chart-on', true);
		}
		/*
		 * bxSlider
		 */

		if ($('[id="slider1"]').length && $('ul[id="slider1"] > li').length) {

			$('.view-my-investment-table').show();
			$('[id="slider1"]').each(function(i, elm) {
				var slider = $(elm);
				var slider1 = slider.bxSlider({
					controls: true,
					speed: 0,

					onNextSlide: function(currentSlide, totalSlides) {},
					onPrevSlide: function(currentSlide, totalSlides) {},
					onBeforeSlide: function(currentSlide, totalSlides) {},
					onAfterSlide: function(currentSlide, totalSlides) {
						var objlink = slider.find('.slider1-control a')[currentSlide];
						slider.find('.slider1-control a').removeClass('selected');
						$(objlink).addClass('selected');
						//$('#slider1 li').css('height', '1px');
						//$('#slider1 li').css('height', '230px');
						slider.find('li').css('height', 'auto,', 'width', '630');


						if (currentSlide == 1) {
							//$('#morelessinvestments').show();
							slider.find('li').css('height', 'auto', 'width', '630');
						} else {
							//$('#morelessinvestments').hide();
							slider.find('li').css('height', 'auto', 'width', '630');
						}

					}

				});


				slider.find('.slider1-control a').click(function() {
					var thumbIndex = $(this);
					slider1.goToSlide(thumbIndex);
					return false;
				});

				//		slider1.goToFirstSlide();
				//		$('.slider1-control a:first').addClass('selected');
				slider.find('.slider1-control a:first').trigger('click');
			});
			$('#view-my-investment-table-large').hide();
			$('#view-my-investment-table-small').hide();
			setTimeout(function(){ // why? the browser chugs for a sec during rendering and it SEEMS this procedure gets skipped only sometimes (Chrome)
				if (chart_container === 'details-chart-large') {
					// check to see if the table is displayed on the small view
					if ($('.view-table-chart-small').text() == 'View Chart') {
						$('.view-table-chart-large').click();
					} else {
						$('#view-my-investment-table-large').hide();
						$('#view-my-investment-table-small').hide();
					}
				} else if (chart_container === 'details-chart-small') {
					// check to see if the table is displayed on the small view
					if (!$('.view-table-chart-large').length) {
						$('#view-my-investment-table-small').hide();
					}
				}
			}, 500);
		}
		var buttons = $('#myInvestmentPanel .viewSwitch button').click(function(event) {
			buttons.removeClass('selected');
			$(this).addClass('selected');
			if ($(this).text() == 'Table') {
				$('.view-my-investment-table').show();
				$('.chart-my-investment, .chart-legend').hide();
			} else {
				$('.view-my-investment-table').hide();
				$('.chart-my-investment, .chart-legend').show();
			}
		});
	};

	TIAAPROTO.utils.portfolio_summary.portsum_table_toggle = function(large_or_small) {
		return function(event) {
			var opposite = (large_or_small == 'large' ? 'small' : 'large');

			if ($(this).text() == 'View Table') {
				$(this).text('View Chart');
				$('.view-my-investment-table').show();
				$('.chart-my-investment, .chart-legend').hide();
			} else {
				$(this).text('View Table');
				$('.view-my-investment-table').hide();
				$('.chart-my-investment, .chart-legend').show();
			}
			$('.view-table-chart-'+opposite).text($(this).text());
		};
	};

	TIAAPROTO.utils.portfolio_summary.asset_class = function(pie_container) {
		if (TIAAPROTO.data.portfolio_summary && TIAAPROTO.data.portfolio_summary.asset_class && $('[id="' + pie_container + '"]').length) {
			// load asset class chart
			VCharts.miniPie({ // Summary Pie
				data: TIAAPROTO.data.portfolio_summary.asset_class,
				render_to: pie_container,
				legend_total: false,
				hc: {
					chart: {
						height: (pie_container.indexOf('large') > -1 ? 200 : 140),
						width: (pie_container.indexOf('large') > -1 ? 200 : 140)
					},
					plotOptions: {
						pie: {
							size: (pie_container.indexOf('large') > -1 ? 200 : 130),
							point: {
								events: {
									mouseOver: function(event) {
										var parent = $('[id="' + pie_container + '"]').closest('.chartContainer');
										var class_assets = parent.find('[data-class-name="' + this.name + '"]').show();
										class_assets.find('tr:first td').css("color", this.color);
										parent.find('[id="asset-class-pie-hover"]').stop(true,true).fadeIn();
									},
									mouseOut: function(event) {
										var parent = $('[id="' + pie_container + '"]').closest('.chartContainer');
										parent.find('[id="asset-class-pie-hover"]').stop(true,true).hide();
										parent.find('[data-class-name="' + this.name + '"]').hide();
									}
								}
							}
						}
					}
				}
			});
		}
	};

	TIAAPROTO.utils.portfolio_summary.account_type = function(pie_container) {
		var data = [];
		for (var i = 0; i < TIAAPROTO.data.portfolio_summary.account_type.length; i++) {
			data.push([TIAAPROTO.data.portfolio_summary.account_type[i].name, TIAAPROTO.data.portfolio_summary.account_type[i].percent])
		}
		var chart_newtwo = new Highcharts.Chart({
			chart: {
				renderTo: pie_container,
				borderWidth: null,
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false,
				width: (pie_container.indexOf('large') > -1 ? 200 : 140),
				height: (pie_container.indexOf('large') > -1 ? 200 : 140)
			},
			credits: {
				enabled: false
			},
			title: {
				text: null
			},
			colors: [
					'#005eb8',
					'#337ec6',
					'#669ed4'
			],
			tooltip: {
				enabled: false
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					showInLegend: false,
					size: (pie_container.indexOf('large') > -1 ? 200 : 130),
					dataLabels: {
						enabled: false
					},
					point: {
						events: {
							mouseOver: function(event) {
								var parent = $('[id="' + pie_container + '"]').closest('.chartContainer');
								var class_name = this.name.replace(/\s+$/, '').replace(/\s/g, '-').toLowerCase();
								var class_assets = parent.find('[data-class-name="' + class_name + '"]').show();
								class_assets.find('tr:first td').css("color", this.color);
								parent.find('[id="account-type-pie-hover"]').stop(true,true).fadeIn();
								/* // removed mousemove to match production.
								var container = parent.find('[id="' + pie_container + '-hover"]').fadeIn();
								if (!this.has_mousemove) {
									$('[id="' + pie_container + '"]').mousemove(function(e) {
										var $this = $(this);
										var offset = $this.offset();
										container.css("top", e.pageY - offset.top - (container.height()/2) + 10 + "px");
										container.css("left", e.pageX - offset.left + 50 + "px");//
									});
									this.has_mousemove = true;
								}
								*/
							},
							mouseOut: function(event) {
								var parent = $('[id="' + pie_container + '"]').closest('.chartContainer');
								//parent.find('[id="' + pie_container + '-hover"]').hide();
								parent.find('[id="account-type-pie-hover"]').stop(true,true).hide();
								parent.find('[data-class-name="' + this.name.replace(/\s/g, '-').toLowerCase() + '"]').hide();
							}
						}
					}
				}
			},
			series: [{
					type: 'pie',
					dataLabels: {
						enabled: false
					},
					name: 'Asset Class',
					data: data
				}
			]
		});
	};

	TIAAPROTO.utils.portfolio_summary.payment_summary = function(pie_container) {
		if ($('[id="' + pie_container + '"]').width()) {
			var income_chart;

			$('[id="' + pie_container + '"]').css("width", "320px");
			try {
				income_chart = new Highcharts.Chart({
					chart: {
						renderTo: pie_container,
						type: 'column',
						width: 320
					},
					colors: [
							'#658D1B',
							'#3480c4'
					],
					credits: {
						enabled: false
					},
					title: {
						text: null
					},
					xAxis: {
						categories: ['Year to Date $5879<br>Taxable $472.73', '2013 $5879<br> Taxable $472.73'],
						plotLines: [{
								color: '#f1f1f1',
								width: 1,
								value: 0.5
							}
						]

					},
					yAxis: {
						min: 0,
						stackLabels: {
							enabled: false
						},
						title: {
							text: null
						},
						tickInterval: 2000,
						labels: {
							formatter: function() {
								return "$" + this.value;
							}
						},
						gridLineColor: '#F1F1F1'
					},
					legend: {
						enabled: false
					},
					tooltip: {
						enabled: false
					},
					plotOptions: {
						column: {
							stacking: 'normal',
							dataLabels: {
								enabled: false
							}
						},
						series: {
							pointWidth: 120
						}

					},
					series: [{
							name: 'Annual Income',
							data: [1470, 2462]
						}, {
							name: 'Payments',
							data: [4409, 7385]
						}
					]
				});
			} catch (e) {}

			//$('[id="' + pie_container + '"]').find('.ret-income-bar-container').append($('[id="' + pie_container + '"]'));
			//$('[id="' + pie_container + '"]').bind("plothover", RIstackHover);

		}
	};

	TIAAPROTO.utils.portfolio_summary.show_hide_investments = function(event){
		event.preventDefault();
		var $this = $(this);
		if ($this.text() == 'View More') {
			$this.parent().prev().css({
				height: 'auto',
				overflow: ''
			});
			$this.text('View Less');
			$this.parent().prev().find('.excel_export').hide().filter(':last').show();
		} else {
			$this.parent().prev().css({
				height: 220,
				overflow: 'hidden'
			});
			$this.text('View More');
			$this.parent().prev().find('.excel_export').hide().filter(':first').show();
		}
	};

	TIAAPROTO.utils.portfolio_summary.ajax_complete = function(in_modal) {
		var process_modal_contents = function() {
			TIAAPROTO.utils.portfolio_summary.my_investments('details-chart-large');
			TIAAPROTO.utils.portfolio_summary.asset_class('piechart_asset_large');
			TIAAPROTO.utils.portfolio_summary.payment_summary('ret-income-bar-large');
			TIAAPROTO.utils.portfolio_summary.account_type('account-type-pie-large');
			TIAAPROTO.utils.portfolio_summary.tab_change_init('#modal-viewLarger');
			var tab_index = $('#portfolio-summary ul.tabs a').index($('#portfolio-summary ul.tabs a.selected'));
			TIAAPROTO.utils.portfolio_summary.tab_change('#modal-viewLarger', tab_index);
			$('#modal-viewLarger ul.tabs a').click(function(event) {
				var tab_index = $('#modal-viewLarger ul.tabs a').index($(this));
				TIAAPROTO.utils.portfolio_summary.tab_change('#portfolio-summary', tab_index);
			});
			// Attach events to account selector triggers
			$('#modal-viewLarger a[href="#accountsMenu"]').on('actionLinkShow', function(event) {
				menu.data('trigger-element', this);
			})
				.on('actionLinkHide', function(event) {
				menu.removeData('trigger-element');
			});
			$('#modal-viewLarger a.viewmorelessinvestments').on('click', TIAAPROTO.utils.portfolio_summary.show_hide_investments);
			$('#modal-viewLarger a[href="#closePopup"]').click(function(event){
				event.preventDefault();
				$('#modal-viewLarger').dialog('close');
			});
		};

		var target = '[id="modal-viewLarger"]';
		var trigger = '#portfolioSummaryContainer a[href="#modal-viewLarger"]';

		return function(data, textStatus, jqXHR) {
			if (in_modal) {
				var $data = $(data);
				var $target = $(target);
				$target.find('.body').remove();
				$target.append($data).append($data.children());
				$data.remove();
				process_modal_contents();
				$target.find('.jq-actions').actionLink()
					.filter('a[href="#accountsMenu"],a[href="#accountsMenuNoClosed"]')
					.on('actionLinkShow', function(event) {
					menu.data('trigger-element', this);
				})
					.on('actionLinkHide', function(event) {
					menu.removeData('trigger-element');
				});
			} else {
				var tab_index = $('#portfolio-summary ul.tabs a').index($('#portfolio-summary ul.tabs a.selected'));
				$('#modal-viewList').remove();
				$('#portfolioSummaryContainer').empty().append(data);
				TIAAPROTO.utils.portfolio_summary.my_investments('details-chart-small');
				TIAAPROTO.utils.portfolio_summary.asset_class('piechart_asset_small');
				TIAAPROTO.utils.portfolio_summary.asset_class('brokerage_piechart_asset_small');
				TIAAPROTO.utils.portfolio_summary.payment_summary('ret-income-bar');
				TIAAPROTO.utils.portfolio_summary.account_type('account-type-pie-small');
				TIAAPROTO.utils.portfolio_summary.tab_change('#portfolio-summary', tab_index);
				$('#portfolioSummaryContainer a.viewmorelessinvestments').on('click', TIAAPROTO.utils.portfolio_summary.show_hide_investments);
				// Attach events to account selector triggers
				$('#portfolioSummaryContainer .jq-actions').actionLink()
					.filter('a[href="#accountsMenu"],a[href="#accountsMenuNoClosed"]')
					.on('actionLinkShow', function(event) {
						menu.data('trigger-element', this);
					})
					.on('actionLinkHide', function(event) {
						menu.removeData('trigger-element');
					});
				// Remove the view larger if it exists and
				// Handle modal triggers within ajax content
				if ($(trigger).length) {
					$(trigger).click(function(event) {
						event.preventDefault();
						TIAAPROTO.utils.modal({
							triggerSelector: trigger,
							targetSelector: target,
							partial: {
								'class': 'Tiaa_View_Iwc_Index',
								template: 'iwc/partials/modals/modal-view-larger'
							},
							success: function() {
								// process new chart
								process_modal_contents();
							}
						});
					});
				}
				TIAA.ui.initModalPopup();
				if ($(target+':visible').length === 0) {
					$(target).remove()
				}
				TIAAPROTO.utils.portfolio_summary.set_default();
				$(document).trigger('tabLoadComplete');
			}
			if (TIAAPROTO.data.portfolio_summary && TIAAPROTO.data.portfolio_summary.account_selected !== -1) {
				if (TIAAPROTO.data.portfolio_summary.account_selected.type.indexOf('closed') > -1) {
					$('a[href="#accountsMenu"],a[href="#accountsMenuNoClosed"]').html('<span class="icon"></span>' + $('.all-accounts a').html());
					$('a[href="#accountsMenuNoClosed"]').html('<span class="icon"></span>' + TIAAPROTO.data.portfolio_summary.account_selected.name);
				} else {
					$('a[href="#accountsMenu"],a[href="#accountsMenuNoClosed"]').html('<span class="icon"></span>' + TIAAPROTO.data.portfolio_summary.account_selected.name + ' ' + accounting.formatMoney(TIAAPROTO.data.portfolio_summary.account_selected.total));
				}
			} else {
				$('a[href="#accountsMenu"],a[href="#accountsMenuNoClosed"]').html('<span class="icon"></span>' + $('.all-accounts a').html());
			}
		};
	};

	TIAAPROTO.utils.portfolio_summary.account_selection = function(account, in_modal) {

		var target = '[id="modal-viewLarger"]';
		var trigger = '#portfolioSummaryContainer a[href="#modal-viewLarger"]';

		if (
			TIAAPROTO.data.portfolio_summary.account_selected.account_group_id
			|| (typeof TIAAPROTO.data.portfolio_summary.account_selected === 'object' && TIAAPROTO.data.portfolio_summary.account_selected.id !== account)
			|| (typeof TIAAPROTO.data.portfolio_summary.account_selected === 'number' && TIAAPROTO.data.portfolio_summary.account_selected !== parseInt(account))
			|| (!in_modal && $('#portfolioSummaryContainer').children().length === 0)
			|| (in_modal && $(target).length === 0)) {

			if (in_modal) {
				TIAAPROTO.utils.portfolio_summary.account_selection(account, false);
			}

			$.ajax({
				url: TIAAPROTO.env.root + 'iwc/account_home/portfolio_summary',
				dataType: 'html',
				type: 'POST',
				data: {
					account: account,
					is_modal: (in_modal ? 'true' : 'false')
				},
				success: TIAAPROTO.utils.portfolio_summary.ajax_complete(in_modal)
			});
		}
	};

	TIAAPROTO.utils.portfolio_summary.group_selection = function(group, in_modal) {

		var target = '[id="modal-viewLarger"]';
		var trigger = '#portfolioSummaryContainer a[href="#modal-viewLarger"]';

		if (
			TIAAPROTO.data.portfolio_summary.account_selected.account_group_id
			|| TIAAPROTO.data.portfolio_summary.account_selected === -1
			|| (typeof TIAAPROTO.data.portfolio_summary.account_selected === 'object' && !TIAAPROTO.data.portfolio_summary.account_selected.account_group_id && TIAAPROTO.data.portfolio_summary.account_selected.id !== group)
			|| (!in_modal && $('#portfolioSummaryContainer').children().length === 0)
			|| (in_modal && $(target).length === 0)) {

			if (in_modal) {
				TIAAPROTO.utils.portfolio_summary.group_selection(group, false);
			}

			$.ajax({
				url: TIAAPROTO.env.root + 'iwc/account_home/portfolio_summary',
				dataType: 'html',
				type: 'POST',
				data: {
					group: group,
					is_modal: (in_modal ? 'true' : 'false')
				},
				success: TIAAPROTO.utils.portfolio_summary.ajax_complete(in_modal)
			});
		}
	};

	TIAAPROTO.utils.portfolio_summary.tab_change_init = function(container) {
		$(container + ' ul.tabs a').click(function(event) {
			$(this).closest('ul').find('div.pointerTop').addClass('hidden');
			$(this).next().removeClass('hidden');
		});
	};

	TIAAPROTO.utils.portfolio_summary.tab_change = function(container, index) {
		if (index > -1 && $(container).length) {
			var anchor = $(container + ' ul.tabs a').removeClass('selected').eq(index).addClass('selected');
			anchor.closest('ul').find('div.pointerTop').addClass('hidden');
			anchor.next().removeClass('hidden');
			$(container + ' ul.tabs a').each(function(i, element){
				$($(element).attr('rel')).hide();
			});
			$(anchor.attr('rel')).fadeTo('slow', 1);
		}
	};

	TIAAPROTO.utils.portfolio_summary.set_default = function() {
		$('#portfolio-summary a[href="#set-default"]').click(function(event){
			event.preventDefault();
			var tab_index = $('#portfolio-summary ul.tabs a').index($('#portfolio-summary ul.tabs a.selected'));
			var self = $(this);
			TIAAPROTO.utils.session('portfolio_summary_default', tab_index, function(){
				$('.default-view-overlay').show().css('opacity', 0).fadeTo('slow', 1).delay(1000).fadeTo('slow', 0, function(){$(this).hide();});
				$('#portfolio-summary div.pageutil').removeClass('default');
				self.closest('div.pageutil').addClass('default');
			});
		});
	};

	TIAAPROTO.utils.account_home = TIAAPROTO.utils.account_home || {};
	TIAAPROTO.utils.account_home.customize_banner = function() {
		// preview
		$('#customize-image-banner a[href="#select-banner"]').click(function(event){
			event.preventDefault();
			var image = $(this).find('img').attr('src').replace(/\-thumb/, '');
			var preview = $('#customize-image-preview').attr('src', image);
			if (image.indexOf('accounthome_no_banner') > -1) {
				preview.css('visibility', 'hidden');
			} else {
				preview.css('visibility', 'visible');
			}
			compute_selected();
		});
		// selected class
		var compute_selected = function() {
			if($('#customize-image-banner').length)
			{
			$('#customize-image-banner img.selected').removeClass('selected');
			var selected_src = $('#customize-image-preview').attr('src');
			selected_src = selected_src.substring(0, selected_src.length - 4);
			$('#customize-image-banner a img[src^="'+selected_src+'"]').addClass('selected');
		}
		};
		compute_selected();
		// save
		var save_banner = function(button){
			// save in db or session via ajax
			var banner = $('#customize-image-preview').attr('src');
			var no_image = (banner.indexOf('accounthome_no_banner.png') > -1);
			if (no_image) {
				banner = false;
			}
			var span = button.fadeTo('slow', .5).find('span').text('Saving...');
			TIAAPROTO.utils.session('banner', banner, function(){
				if (banner) {
					$('#banner-container').removeClass('no-image').find('#banner').attr('src', banner).show();
				} else {
					$('#banner-container').addClass('no-image').find('#banner').hide();
				}
				button.fadeTo('slow', 1);
				span.text('Save Changes');
				$('#modal-customize').dialog('close');
			});
			TIAAPROTO.utils.session('no_image_banner', no_image);
		};

		$('#customize-image-banner a[href="#save-banner"]').click(function(e){
			e.preventDefault();
			save_banner($(this));
		});


		//	@TODO: Fix TCW popup code
		// 	Note: I needed to manually add the "keypress" event binding (even though JQ's .click should handle it just fine).
		//	Something in the TCW popup code is doing some funky things with keypress bindings on links that launch popups.
		//	The result is that we're unable to capture the keypress event (when only using .click())

		$('#banner-container a[href="#modal-customize"][rel="#customize-image-banner"]').on('click keypress',function(e){
			if ((e.type == 'click') || (e.type == 'keypress' && e.which == 13)){
				e.preventDefault();
				$('#modal-customize ul.vtabs a[rel="#customize-image-banner"]').click();
			}

		});

		$('#customize-image-banner a[href="#close"]').click(function(e){
			e.preventDefault();
			$('#modal-customize').dialog('close');
		});


	};




		TIAAPROTO.utils.account_home.customize_accounts = function() {

			var account_group_order = []
			, 	account_nicknames = {}
			, 	default_account_group_order = {}
			, 	default_account_nicknames = {}
			,	$list = $('#customizable-account-groups')
	    	, 	$pointer = $('#sort-pointer')
	    	,	keys = {esc:27, tab:9, left:37, up:38, right:39, down:40, m:77}
	    	,	drag_state = false
	     	, 	drag_top
	  	    , 	$drag_node
			;

			// Common/non transactional event bindings

			//	@TODO: Fix TCW popup code
			// 	Note: I needed to manually add the "keypress" event binding (even though JQ's .click should handle it just fine).
			//	Something in the TCW popup code is doing some funky things with keypress bindings on links that launch popups.
			//	The result is that we're unable to capture the keypress event (when only using .click())

			$('#accountSummary a[href="#modal-customize"][rel="#customize-account-name"]').on('click keypress',function(e){
				if ((e.type == 'click') || (e.type == 'keypress' && e.which == 13)){
					e.preventDefault();
					$('#modal-customize ul.vtabs a[rel="#customize-account-name"]').click();
				}

			});

			// $('#customize-account-name a[href="#close"]').on('click',function(e){
			// 	e.preventDefault();
			// 	$('#modal-customize').dialog('close');
			// });


			// default data grabs
				// default account group order
			$('#customize-account-name-form .customizable-account-group').each(function(i) {
			    default_account_group_order[i] = $(this).attr('data-defaultOrder');
			});

				// default account nicknames
			$('#customize-account-name-form input[type=text]').each(function(i) {
			    default_account_nicknames[$(this).attr('name')] = $(this).attr('data-defaultName');

			});

		 // Setup account group sorting / reorder functionality
  	 	$list.sortable({
			axis: 'y',
			containment: 'parent',
			opacity: 1,
			forcePlaceholderSize: true,
			forceHelperSize: true,
			placeholder: 'shd4',
			cursor: 'move',
			items: '.customizable-account-group',
			handle: '.hd',
           	create: function( event, ui ) {
					// grab starting order
					$(this).children('.customizable-account-group').each(function(i) {
					  	account_group_order.push($(this).attr('data-accountId'));
					  	$(this).attr({'aria-grabbed':'false','tabindex':'0'});
					});
				},

			update: function(event, ui) {
				// reassign sortOrder with new account sort order
	    		 account_group_order = $(this).sortable('toArray', {attribute: 'data-accountId' });
			 },

           	helper: function(event,el){
					var copy = $(el).clone().css({
						width: $(el).width(),
						height: $(el).height()
					});


					copy.css({
						backgroundColor: '#fffeee',
						borderLeft: '1px solid #D6D6D6',
						borderRight: '1px solid #D6D6D6'
					});


					var container = $('<div id="customizable-account-groups"></div>').append(copy);
					return copy;
				},

           	start: function (event, ui) {
              var $item = $(ui.item)
              	, $helper = $(ui.helper)
              	;

              $item.focus();

              $helper.fadeTo('slow', 0.3);
              $helper.attr('aria-grabbed', 'true');

           }, // end start method
           stop: function (event, ui) {
              var $item = $(ui.item)
              	, $helper = $(ui.helper)
              	;

              $item.focus();

              $helper.fadeTo('slow', 1);
              $helper.attr('aria-grabbed', 'false');

           } // end stop method

       });

	   $list.children('.customizable-account-group').on({
	    'keydown' : function(e){
	      var $item = $(this)
	        , $listItems = $item.siblings('.customizable-account-group')
	        , $next_item;

	           switch (e.keyCode) {
	               case keys.up:
	               case keys.left:

	                       if (drag_state) {

	                       $next_item = $item.prev();


	                           if ($next_item.length) {
	                               $item.attr('tabindex', -1);
	                               $next_item.attr('tabindex', 0).focus();

	                               if (drag_state) {
	                                   $pointer.css({
	                                       'top': $next_item.position().top,
	                                       'left': '-15px'
	                                   });
	                               } // endif

	                           } // endif



	                       e.stopPropagation();
	                       e.preventDefault();
	                       return false;

	                   }
	                   break;

	               case keys.down:
	               case keys.right:


	                       if (drag_state) {

	                       	$next_item = $item.next();


	                           if ($next_item.length) {
	                               $item.attr('tabindex', -1);
	                               $next_item.attr('tabindex', 0).focus();

	                               if (drag_state) {
	                                    $pointer.css({
	                                       'top': $next_item.position().top,
	                                       'left': '-15px'
	                                   });
	                               } // endif

	                           }


	                       e.stopPropagation();
	                       e.preventDefault();
	                       return false;


	                   } // endif
	                   break;

	               case keys.m:

	                   if (e.ctrlKey && !e.altKey) {

	                       if (drag_state) {

	                           if ($item[0] != $drag_node[0]) {

	                              if ($item.position().top > drag_top) {
	                                   $item.after($drag_node);
	                               } else {
	                                   $item.before($drag_node);
	                               } // endif
	                           } // endif

	                           drag_state = false;

	                           $item.attr('aria-grabbed', 'false');
	                           $listItems.attr('aria-grabbed', 'false').removeAttr('aria-dropeffect');
	                           $drag_node.removeClass('key-drag').focus();
	                           $pointer.css({
	                               'top': -300,
	                               'left': -300
	                           });
	                       } else {
	                           drag_state = true;
	                           drag_top = $item.position().top;
	                           $drag_node = $item;

	                           $item.attr('aria-grabbed', 'true');
	                           $listItems.attr('aria-dropeffect', 'move');
	                           $drag_node.addClass('key-drag');
	                            $pointer.css({
	                               'top': $drag_node.position().top,
	                               'left': '-15px'
	                           });

	                       } // endif

	                       e.stopPropagation();
	                       e.preventDefault();
	                       return false;

	                   } // endif
	                   break;

	               case keys.esc:
	                   if (drag_state) {

	                       drag_state = false;
	                       $item.attr('aria-grabbed', 'false');
	                       $listItems.attr('aria-grabbed', 'false').removeAttr('aria-dropeffect');
	                       $drag_node.removeClass('key-drag').focus();
	                       $pointer.css({
	                           'top': -300,
	                           'left': -300
	                       });

	                       e.stopPropagation();
	                       e.preventDefault();
	                       return false;
	                   } // endif
	                   break;

	               case keys.tab:
	                   if (drag_state) {

	                       drag_state = false;
	                       $item.attr('aria-grabbed', 'false');
	                       $listItems.attr('aria-grabbed', 'false').removeAttr('aria-dropeffect');
	                       $pointer.css({
	                           'top': -300,
	                           'left': -300
	                       });
	                       $drag_node.removeClass('key-drag');

	                   } // endif

	                   break;

	               } // end switch

                  return true;
	       }

	   });




		// save all account customizations
		var save_account_customization = function(button){
			var button = button
			,	span = button.fadeTo('slow', .5).find('span').text('Saving...');

			// success callback for session requests
			var onSessionSave = function(){
				// let's keep the text in "saving" status as the window will be reloaded anyways

				// button.fadeTo('slow', 1);
				// span.text('Save Changes');

				// for some reason the onSessionSave/.done() method is called before the calls actually finish?
				// let's slow it down a bit.
      			var timer = setInterval((function(){
					$('#modal-customize').dialog('close');
					window.location.reload(true);
					clearInterval(timer);

      			}), 500);



			}

			var onSessionError = function(){
				// console.log('error');
			}

			//Re-Grab the account nickname fields
			$('#customize-account-name-form input[type=text]').each(function(i) {
			    account_nicknames[$(this).attr('name')] = $(this).val();

			});

    		$.when(
				// Save account group sort order in session
				TIAAPROTO.utils.session('account_group_order', account_group_order),
				// Save account nicknames in session
				TIAAPROTO.utils.session('account_nicknames', account_nicknames)
			).then(onSessionSave,onSessionError);


		}; // save_account_customization()




		// reset all account customizations
		var reset_account_customization = function(){


			//Reset to the default account nicknames and (eventually) sort order

			// restore default account nicknames
			$('#customize-account-name-form input[type=text]').each(function(i) {
				var input = $(this);

			    if (input.val() !== default_account_nicknames[input.attr('name')]){
			    	input.val(default_account_nicknames[input.attr('name')]);
			    	// input.parent().parent().effect("highlight", {}, 3000);
			    }
			});


		}; // reset_account_customization()





			$('#customize-account-name a[href="#close"]').click(function(e){
				e.preventDefault();
				$('#modal-customize').dialog('close');
				reset_account_customization();
			});


		$('#customize-account-name a[href="#save-accounts"]').click(function(e){
			e.preventDefault();
			save_account_customization($(this));

		});


	};


	TIAAPROTO.utils.account_home.retirement_rtq_agreement = function() {

		var hasAgreed = false
		, 	$rtqModal = $('#retirement-rtq-vendor-agreement-popup')
		,	$acceptBtn = $rtqModal.find('#retirement-rtq-accept-btn')
		,	$cancelBtn = $rtqModal.find('a[href="#closePopup"]')
		,	$rtqTermsCB = $rtqModal.find('#retirement-rtq-terms')
		;

		/* Real Time Quote Modal Functionality */
		$rtqTermsCB.click(function(e){
			var state = $(this).prop('checked');
			if (state){
				$acceptBtn.removeClass('btnOff');
				hasAgreed = true;
			} else {
				$acceptBtn.addClass('btnOff');
				hasAgreed = false;
			}

		});

 		$cancelBtn.click(function(e){
 			var state = $rtqTermsCB.prop('checked');

			if (state){
				$rtqTermsCB.prop('checked',false);
				$acceptBtn.addClass('btnOff');
				hasAgreed = false;
				$rtqModal.dialog('close');
			} else {
				$rtqModal.dialog('close');
			}

 		});


		 $acceptBtn.click(function(e){
			e.preventDefault();

			var state = $rtqTermsCB.prop('checked');

			if (state){
				save_rtq_agreement($(this));
			} else {
				return false;
			}


		});


		// save all account customizations
		var save_rtq_agreement = function(button){
			var button = button
			,	span = button.fadeTo('slow', .5).find('span').text('Saving...');

			// success callback for session requests
			var onSessionSave = function(){
				// let's keep the text in "saving" status as the window will be reloaded anyways

				// button.fadeTo('slow', 1);
				// span.text('Save Changes');

				// for some reason the onSessionSave/.done() method is called before the calls actually finish?
				// let's slow it down a bit.
      			var timer = setInterval((function(){
					$rtqModal.dialog('close');
					window.location.reload(true);
					clearInterval(timer);

      			}), 500);



			}

			var onSessionError = function(){
				// console.log('error');
			}


    		$.when(
				// Save account group sort order in session
				TIAAPROTO.utils.experience('retirement_rtq_acknowledged', hasAgreed)
			).then(onSessionSave,onSessionError);


		}; // save_rtq_agreement()

	}


	TIAAPROTO.utils.account_home.brokerage_rtq_agreement = function() {

		var hasAgreed = false
		, 	$rtqModal = $('#brokerage-rtq-vendor-agreement-popup')
		,	$acceptBtn = $rtqModal.find('#brokerage-rtq-accept-btn')
		,	$cancelBtn = $rtqModal.find('a[href="#closePopup"]')
		,	$rtqTermsCB = $rtqModal.find('#brokerage-rtq-terms')
		;

		/* Real Time Quote Modal Functionality */
		$rtqTermsCB.click(function(e){
			var state = $(this).prop('checked');
			if (state){
				$acceptBtn.removeClass('btnOff');
				hasAgreed = true;
			} else {
				$acceptBtn.addClass('btnOff');
				hasAgreed = false;
			}

		});

 		$cancelBtn.click(function(e){
 			var state = $rtqTermsCB.prop('checked');

			if (state){
				$rtqTermsCB.prop('checked',false);
				$acceptBtn.addClass('btnOff');
				hasAgreed = false;
				$rtqModal.dialog('close');
			} else {
				$rtqModal.dialog('close');
			}

 		});


		 $acceptBtn.click(function(e){
			e.preventDefault();

			var state = $rtqTermsCB.prop('checked');

			if (state){
				save_rtq_agreement($(this));
			} else {
				return false;
			}


		});


		// save all account customizations
		var save_rtq_agreement = function(button){
			var button = button
			,	span = button.fadeTo('slow', .5).find('span').text('Saving...');

			// success callback for session requests
			var onSessionSave = function(){
				// let's keep the text in "saving" status as the window will be reloaded anyways

				// button.fadeTo('slow', 1);
				// span.text('Save Changes');

				// for some reason the onSessionSave/.done() method is called before the calls actually finish?
				// let's slow it down a bit.
      			var timer = setInterval((function(){
					$rtqModal.dialog('close');
					window.location.reload(true);
					clearInterval(timer);

      			}), 500);



			}

			var onSessionError = function(){
				// console.log('error');
			}


    		$.when(
				// Save account group sort order in session
				TIAAPROTO.utils.experience('brokerage_rtq_acknowledged', hasAgreed)
			).then(onSessionSave,onSessionError);


		}; // save_rtq_agreement()

	}



	$(document).bind('dc-after-load', function() {
		$('#groups').find('a.expander').click(function(event) {
			event.preventDefault();
			$(this).closest('.cm').find('.acctlist').slideToggle('fast');
			// toggle thin bricklet footnote
			$(this).closest('section').next('section.brick-footnote').slideToggle('fast').prev('section.drop').toggleClass('mb');
			// display thin brick view when collapsed, orig view when expanded - for Retirement bricklet only.
			// $(this).closest('section').find('.group-thin-bricklet, .group-orig-bricklet').toggle();
			$(this).toggleClass('expanded collapsed');
		});
		var mouseOver = function(event) {
			$('[data-class-name="' + this.name + '"]').show().closest('.infoHover').fadeIn();
		};
		var mouseOut = function(event) {
			$('[data-class-name="' + this.name + '"]').hide().closest('.infoHover').hide();
		};

		if (TIAAPROTO.data && TIAAPROTO.data.account_home && TIAAPROTO.data.account_home.groups) {
			for (var i = 1; i < TIAAPROTO.data.account_home.groups.length; i++) {
				VCharts.miniPie({ // Summary Pie
					data: TIAAPROTO.data.account_home.groups[i],
					render_to: 'piechart-' + i,
					legend_container: 'piechart-legend-' + i,
					legend_total: true,
					hc: {
						chart: {
							width: 88, //180,
							height: 88 //180
						},
						plotOptions: {
							pie: {
								size: 80,
								point: {
									events: {
										mouseOver: mouseOver,
										mouseOut: mouseOut
									}
								}
							}
						}
					}
				});
			}
		}
		$('#groups').find('a.summaryexpander').off('.globalExpandCollapse').on('click', function(event) {
			event.preventDefault();
			$(this).closest('li').toggleClass('bgexpanded').find('.detail').slideToggle('fast');
			$(this).toggleClass('expanded collapsed');
			if (!$(this).data('chart_on')) {
				var id = $(this).attr('data-id');
				VCharts.miniPie({
					data: ($(this).attr('data-use-ps') == 'true' ? TIAAPROTO.data.portfolio_summary.asset_class : TIAAPROTO.data.account_home.accounts[id]),
					render_to: 'barchart-' + id,
					legend_container: 'barchart-legend-' + id,
					legend_total: false,
					hc: {
						chart: {
							width: 88, //180,
							height: 88 //180
						},
						plotOptions: {
							pie: {
								size: 80
							}
						}
					}
				});
				$('#barchart-legend-'+id+' table.chartTable tr td:nth-child(2)').each(function(i, element){
					if ($(this).text() == 'Multi-Asset') {
						$(this).append('<input type="button" data-tooltip-width="220" aria-required="false" aria-describedby="help-multiAsset" role="button" value="?" class="tipLink" name="tooltip-multiAsset" id="tooltip-multiAsset" title="">'
						+ '<div aria-hidden="true" role="tooltip" id="help-multiAsset" class="tipContents hidden">'
						+ '<div>A multi-asset investment may include holdings in several asset classes such as equities, money market and fixed income. To view how your fund breaks down and a detailed profile, click the fund name wherever you see it.</div>'
						+ '</div>');
					} else if ($(this).text() == 'Guaranteed') {
						$(this).append('<br /><a class="nextLink" href="../iwc/interest-rates.html" title="View Interest Rates"><span class="icon"></span>View interest rates</a>');
					}
				});
				// alter the legend for multi-asset and guaranteed entries - they need a View Interest link
				$(this).data('chart_on', true);
			}
		});



	TIAAPROTO.utils.account_home.updateCommCenterUnreadCount = function(e) {

            var _updateTotalCount,
            	_setCount,
            	_totalUnreadCount = 0,
            	_flyoutLink = $('#messagesFlyoutLink'),
            	_flyoutLinkCounter = _flyoutLink.find('.unreadCount'),
             	_flyoutContainer = $('#messagesFlyout'),
             	_flyoutHeaderGroups = _flyoutContainer.find('h3');


                // Get initial total unread count for each group, update value
                _flyoutHeaderGroups.each(function(i,v){
                	var _totalGroupUnreadCount = _flyoutContainer.find('ul:eq('+i+') li a').length,
 						_flyoutGroupUnread = _flyoutContainer.find('.unreadCountWrap:eq('+i+')').parent().parent();

                    if (_totalGroupUnreadCount > 0){
                        _totalUnreadCount += _totalGroupUnreadCount;
                        _flyoutContainer.find('.unreadCount:eq('+i+')').text(_totalGroupUnreadCount);
                        _flyoutGroupUnread.next('p').hide();
                        _flyoutGroupUnread.nextAll('ul').eq(0).show();

                    } else {
                        _flyoutContainer.find('.unreadCountWrap:eq('+i+')').addClass('hidden');
                        _flyoutGroupUnread.next('p').show();
                        _flyoutGroupUnread.nextAll('ul').eq(0).hide();

                    }

                  });

                	// update the header messages link unread total(for all groups)
                    if (_totalUnreadCount > 0){
                        _flyoutLinkCounter.parent().removeClass('hidden');
                       	_flyoutLinkCounter.text(_totalUnreadCount);

                    } else {
                       _flyoutLinkCounter.parent().addClass('hidden');
                    }


    }







		TIAAPROTO.utils.account_home.customize_banner();
		TIAAPROTO.utils.account_home.customize_accounts();
		TIAAPROTO.utils.account_home.retirement_rtq_agreement();
		TIAAPROTO.utils.account_home.brokerage_rtq_agreement();
		TIAAPROTO.utils.account_home.updateCommCenterUnreadCount();

	});
	TIAAPROTO.utils.portfolio_summary.tab_change_init('#portfolio-summary');
	$('#portfolio-summary ul.tabs a').click(function(event) {
		var tab_index = $('#portfolio-summary ul.tabs a').index($(this));
		TIAAPROTO.utils.portfolio_summary.tab_change('#modal-viewLarger', tab_index);
	});
	if ($('div#accountsMenu,div#accountsMenuNoClosed').length) {
		var account;
		if (TIAAPROTO.data.portfolio_summary && TIAAPROTO.data.portfolio_summary.account_selected) {
			if (typeof TIAAPROTO.data.portfolio_summary.account_selected === 'number') {
				account = TIAAPROTO.data.portfolio_summary.account_selected;
			} else {
				account = TIAAPROTO.data.portfolio_summary.account_selected.id;
			}
		} else {
			TIAAPROTO.data.portfolio_summary.account_selected = account = -1;
		}
		if ((typeof TIAAPROTO.data.portfolio_summary.account_selected === 'object' && TIAAPROTO.data.portfolio_summary.account_selected.account_group_id) || typeof TIAAPROTO.data.portfolio_summary.account_selected === 'number' || account === -1) {
			TIAAPROTO.utils.portfolio_summary.account_selection(account, false);
		} else {
			TIAAPROTO.utils.portfolio_summary.group_selection(account, false);
		}
		var is_in_modal = function(elm) {
			var trigger = $(elm).closest('div#accountsMenu, div#accountsMenuNoClosed').data('trigger-element');
			return (trigger && $(trigger).closest('.popup').length === 1);
		};
		var menu = $('div#accountsMenu, div#accountsMenuNoClosed');
		menu.find('.all-accounts a').click(function(event) {
			event.preventDefault();
			// switch data for all accounts
			TIAAPROTO.utils.portfolio_summary.account_selection(-1, is_in_modal(this));
			var tab_index = $('#portfolio-summary ul.tabs a').index($('#portfolio-summary ul.tabs a.selected'));
			TIAAPROTO.utils.portfolio_summary.tab_change('#portfolio-summary', tab_index);
		});
		menu.find('.group a').click(function(event) {
			event.preventDefault();
			// switch data for group
			var group_id = $(this).attr('data-id');
			TIAAPROTO.utils.portfolio_summary.group_selection(group_id, is_in_modal(this));
		});
		menu.find('.group-account a').click(function(event) {
			event.preventDefault();
			// switch data for account
			var account_id = $(this).attr('data-id');
			TIAAPROTO.utils.portfolio_summary.account_selection(account_id, is_in_modal(this));
		});
		$('a[href="#accountsMenu"],a[href="#accountsMenuNoClosed"]').on('actionLinkShow', function(event) {
			menu.data('trigger-element', this);
		})
			.on('actionLinkHide', function(event) {
			menu.removeData('trigger-element');
		});
	}
})();

/*Grant Permission Functionality*/
$(function(){
	var grantPermissionModal,
		$modalGrantPermission = $('#modal-grantPermission');

	$('#grantPermissionBtn,#changePermissions').on('click',function(e){
		e.preventDefault();
		grantPermissionModal = CreateModal($('#modal-grantPermission'),700).dialog('open');
		$modalGrantPermission.find('ul.acctlist > li').not(':first').each(function(){
			var $li = $(this),
				id = $li.attr('data-id'),
				$liBricklet = $('#externalAccounts').find('[data-extid="'+id+'"]'),
				state = $liBricklet.is(':visible');
			if(state){
				$li.find('.ynSwitch .yes button').addClass('selected');
				$li.find('.ynSwitch .no button').removeClass('selected');
			} else {
				$li.find('.ynSwitch .no button').addClass('selected');
				$li.find('.ynSwitch .yes button').removeClass('selected');
			}
		});
	});

	$modalGrantPermission.find('a[title="Okay"]').on('click',function(e){
		e.preventDefault();
		var $360Section = $('#externalAccounts'),
			$liLiability = $('.ext-liability-acct');

		if($modalGrantPermission.find('ul.acctlist > li:first button[title="Yes"]').hasClass('selected') || $modalGrantPermission.find('ul.acctlist > li:first button[title="No"]').hasClass('selected') || ($modalGrantPermission.find('ul.acctlist > li.acctitem button.selected').size() == $modalGrantPermission.find('ul.acctlist > li.acctitem').size()))	{
			$('#grant-permission').hide();
		} else {
			$('#grant-permission').show();
		}
		$liLiability.show();
		$modalGrantPermission.find('ul.acctlist > li').not(':first').each(function(){
			var $li = $(this),
				id = $li.attr('data-id'),
				$liBricklet = $360Section.find('[data-extid="'+id+'"]'),
				state = $li.find('.ynSwitch button.selected').text().trim() == 'Yes' ? true : false;

			if(state){
				$liBricklet.show();
			} else {
				$liBricklet.hide()
			}
		});
		if(!$liLiability.find('li:visible').length){
			$liLiability.hide();
		}
		grantPermissionModal.dialog('close');
	});

	$('#modal-grantPermission a[title="Cancel"]').on('click',function(e){
		e.preventDefault();
		grantPermissionModal.dialog('close');
	});

	$('#modal-grantPermission ul.acctlist > li:first button[title="Yes"]').on('click',function(e) {
		e.preventDefault();
		$('#modal-grantPermission ul.acctlist > li.acctitem button[title="No"]').removeClass('selected');
		$('#modal-grantPermission ul.acctlist > li.acctitem button[title="Yes"]').addClass('selected');
	});

	$('#modal-grantPermission ul.acctlist > li:first button[title="No"]').on('click',function(e){
		e.preventDefault();
		$('#modal-grantPermission ul.acctlist > li.acctitem button[title="Yes"]').removeClass('selected');
		$('#modal-grantPermission ul.acctlist > li.acctitem button[title="No"]').addClass('selected');
	});
});

$(document).bind('swithTriggered', function(){
		if($('#modal-grantPermission ul.acctlist > li.acctitem button[title="Yes"].selected').size() == $('#modal-grantPermission ul.acctlist > li.acctitem').size())
		{
			$('#modal-grantPermission ul.acctlist > li:first button[title="Yes"]').addClass('selected');
		}
		else
		{
			$('#modal-grantPermission ul.acctlist > li:first button[title="Yes"]').removeClass('selected');
		}
		if($('#modal-grantPermission ul.acctlist > li.acctitem button[title="No"].selected').size() == $('#modal-grantPermission ul.acctlist > li.acctitem').size())
		{
			$('#modal-grantPermission ul.acctlist > li:first button[title="No"]').addClass('selected');
		}
		else
		{
			$('#modal-grantPermission ul.acctlist > li:first button[title="No"]').removeClass('selected');
		}
});

// for expand/collapse +/- table rows
function showHideTRs(event, objID, isVisible, imgID, linkid) {
	if(!linkid) {
		var linkobj = $(event.target);
	} else {
		var linkobj = $(linkid);
	}
	//var objBlock = document.getElementById(objID);
	if(jQuery.browser == "Microsoft Internet Explorer") {
		blockStyle = "block";
	} else {
		blockStyle = "table-row";
	}

	$(objID).children(":first");
	$(objID).prev().find('a:first').toggleClass('collapsed expanded');
	$(objID).toggle('fast', function() {
		//if (!$(this).hasClass('assetdetail')) { // if the expanded row is not asset class detail then apply bg color
		$(this).find('td:first').toggleClass('bgExpandedBottom');
		$(this).prev().find('td').toggleClass('bgExpandedTop');
		//}
	});
}

$(function(){
	// pageutil Whats New Unread/Read indicator
	$('#whatsNewFlyout a.closeLink').click(function(e) {
		e.preventDefault();
		$('#whatsNewFlyout').hide().addClass('hidden');
	});

	/*KBA Questionnaire Step Up - Updates*/
	if(params.velocityKBAFailed)
	{
		$('a[href*="lumpsums/manage-withdrawals.html"],a[href*="lumpsums/getting-started.html"]').addClass('disabled').attr('title','Authentication was not successful. Please call 800 842-2252.');
	}
});

/*
 * Banking Integration Updates
 * Lazy load banking bricklet data
 */

TIAAPROTO.utils.account_home.banking_balance = function() {
	var bankingDataStatus = $('#banking-group-total-value').attr('data-status'),
		bankingLoading = $('.banking-group-loading'),
		bankingTotal = $('#banking-group-total-value'),
		bankingContent = $('#banking-group-content');


	if (bankingDataStatus == 'available'){

		setTimeout(function(){

			bankingLoading.addClass('hidden');
			bankingTotal.add(bankingContent).removeClass('hidden');
			if (typeof TIAAPROTO.env.experience_obj['mytc-thin-brick-hide-bricks'] === 'object' && typeof TIAAPROTO.env.experience_obj['mytc-thin-brick-hide-bricks'].is['true'] === 'boolean') {
				$('#groups .expander').click();
				$('#groups .noticeModule.visible').closest('section').find('.expander').click();
			}

		}, 5000);


	} else if (bankingDataStatus == 'unavailable'){

		setTimeout(function(){
			bankingLoading.addClass('hidden');
			if (typeof TIAAPROTO.env.experience_obj['mytc-thin-brick-hide-bricks'] === 'object' && typeof TIAAPROTO.env.experience_obj['mytc-thin-brick-hide-bricks'].is['true'] === 'boolean') {
				$('#groups .expander').click();
				$('#groups .noticeModule.visible').closest('section').find('.expander').click();
			}
		}, 5000);


	} else {

		// data unavailable

			bankingLoading.addClass('hidden');
			if (typeof TIAAPROTO.env.experience_obj['mytc-thin-brick-hide-bricks'] === 'object' && typeof TIAAPROTO.env.experience_obj['mytc-thin-brick-hide-bricks'].is['true'] === 'boolean') {
				$('#groups .expander').click();
				$('#groups .noticeModule.visible').closest('section').find('.expander').click();
			}
	}
}


$(function(){
	TIAAPROTO.utils.account_home.banking_balance();
});


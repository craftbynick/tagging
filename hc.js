var VCharts = {};

/**
 * Options: {data, render_to, legend_container, legend_total}
 **/
VCharts.miniPie = function(options) {
	if ($('#'+options.render_to).length) {
		var cdata = [];
		var fdata = [];
		var total = options.data[options.data.length-1].total;
		for (var i = options.data.length-1; i--;) { // length - 1 to skip the total at the end
			fdata[options.data[i].account] = options.data[i].balance;
			cdata[i] = {name: options.data[i].account, y: options.data[i].balance/total};
			if (options.data[i].color) {
				cdata[i].color = options.data[i].color;
			}
			if (ColorMap(options.data[i].account)) {
				cdata[i].color = ColorMap(options.data[i].account);
			}
		}
		try{
			var chart_options = $.extend(true, {
				chart: {
					renderTo: options.render_to,
					backgroundColor: 'transparent',
					plotBackgroundColor: 'transparent',
					plotBorderWidth: 0,
					plotShadow: false,
					width: 88,
					height: 88,
					marginRight: 0,
					marginLeft: 0,
					marginBottom: 0,
					marginTop: 0,
					spacingTop: 0,
					spacingRight: 0,
					spacingLeft: 0,
					spacingBottom: 0,
					events: {
						load: function(event) {
							var series = this.get(options.legend_container);
							var tb = $('#'+options.legend_container+' table.chartTable tbody');
							var running_total = 0.0;
							for (var i = series.points.length; i--;) {
								var color = series.points[i].color;
								var y = series.points[i].y;
								var name = series.points[i].name;
								var cash = fdata[name];
								var html = $('<tr data-point="'+i+'" class="ps nbdr alignt"><td><span class="piecl1 mrs" style="background-color:'+color+';color:'+color+';"></span></td><td>'+name+'</td><td class="percentage"><span class="dollars">'+accounting.formatMoney(cash)+'</span></td></tr>');
								html.data('point', series.points[i]);
								running_total += y;
								tb.prepend(html);
							}
							if (options.legend_total) {
								tb.append('<tr class="ps nbdr"><td></td><td><strong>Total</strong></td><td class="percentage"><strong>'+accounting.formatMoney(total)+'</strong></td></tr>');
							}
						}
					}
				},
				credits: {
					enabled: false
				},
				title: {
					text: null
				},
				tooltip: {
					enabled: true,
					formatter: function() {
						var s = '<div class="infoHover pointerLeft" style="width:200px;border:1px solid #d6d6d6;padding:10px;box-shadow:1px 2px 10px -1px #cccccc;background-color:#ffffff;"><div class="pointer" style="margin-top:-10px;margin-left:-20px;"></div>' + this.point.name +'<b><br /> '+ Math.round(this.percentage) +' %' + '</b>';

						//return '<b>';

						s += '</div>';
						return s;
					},
					positioner: function(boxWidth, boxHeight, point) {
			            return {
			                x: point.plotX + 0,
			                y: point.plotY + -30
			            };
			        },
					backgroundColor: 'transparent',
					borderColor: 'transparent',
					borderWidth: 0,
					borderRadius: 0,
					shadow: false,
					useHTML: true,
					style: {
						fontFamily: 'Arial',
						fontSize: '12px',
						padding: '10px',
						lineHeight: '15px'
					}
				},
				legend: {
					align: 'right',
					layout: 'vertical',
					verticalAlign: 'top',
					borderWidth: 0,
					enabled: false
				},
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
							enabled: false
						},
						size: 80,
						borderWidth: 1,
						slicedOffset: 4,
						showInLegend: true
						/*point: {
							events: {
								mouseOver: function(event) {
									var myY = Math.floor(this.y*100);
									var myName = this.name.toUpperCase();
									if ($('.pieHoverContent3').length == 0) {
										$('body').append('<div class="pieHoverContent3"></div>');
									}
									if (!this._mouse_move) {
										this.mouse_move = function(e){
											var Money = total*myY/100;
											var offset = $(this).offset();
											var height = $(this).height();
											$('.pieHoverContent3').css('position', 'absolute');
											$('.pieHoverContent3').css('top', e.pageY+10 + 'px');
											$('.pieHoverContent3').css('left', e.pageX+10 + 'px');
											$('.pieHoverContent3').html(myY + '%' + ' ' + myName + '<br><strong>' + accounting.formatMoney(Money) + '</strong>');
										};
									}
									$('#'+options.render_to).mousemove(this.mouse_move);
								    $(".pieHoverContent3").css("display","block");
								},
								mouseOut: function(event) {
									$('#'+options.render_to).unbind('mousemove', this._mouse_move);
									$(".pieHoverContent3").css("display","none");
								}
							}
						}*/
					}
				},
			    series: [{
					type: 'pie',
					id: options.legend_container,
					name: 'Browser share',
					data: cdata
				}]
			}, options.hc || {});
			var chart = new Highcharts.Chart(chart_options);
			return chart;
		} catch (e) {}
	}
}

/**
 * Options: {data, render_to, legend_container, legend_total}
 **/
VCharts.bar = function(options) {
	if ($('#'+options.render_to).length) {
		var chart;
		var colors = Highcharts.getOptions().colors;
		var cdata = [];
		var categories = [];
		var total = options.data[options.data.length-1].total;
		for (var i = options.data.length-1; i--;) { // length - 1 to skip the total at the end
			cdata[i] = {name: options.data[i].account, y: (options.data[i].balance), color: colors[i]};
			categories[i] = options.data[i].account;
		}
		try{
		chart = new Highcharts.Chart({
			chart: {
				renderTo: options.render_to,
				type: 'column',
				width: 340,
				height: 200,
				events: {
					load: function(event) {
						var series = this.get(options.legend_container);
						var tb = $('#'+options.legend_container+' table.chartTable tbody');
						var running_total = 0.0;
						for (var i = series.points.length; i--;) {
							var color = series.points[i].color;
							var name = series.points[i].name;
							var cash = series.points[i].y;
							var html = $('<tr data-point="'+i+'"><td><span class="piecl1" style="background-color:'+color+';color:'+color+';"></span></td><td>'+name+'</td><td class="percentage"><span class="dollars">'+accounting.formatMoney(cash)+'</span><span class="pct">'+(parseInt((cash/total) * 100000)/1000)+'%</span></td></tr>');
							html.data('point', series.points[i]);
							running_total += cash;
							tb.prepend(html);
							html.bind('mouseover', function(event){
								$(this).addClass('over');
							});
							html.bind('mouseout', function(event){
								$(this).removeClass('over');
							});
						}
						if (options.legend_total) {
							tb.append('<tr class="ps nbdr"><td></td><td><strong>Total</strong></td><td class="percentage"><strong>'+accounting.formatMoney(total)+'</strong></td></tr>');
						}
					}
				}
			},
			title: {
				text: null
			},
			subtitle: {
				text: null
			},
			credits: {
				enabled: false
			},
			xAxis: {
				categories: categories,
				labels: {
					rotation: -45,
					align: 'right'
				}
			},
			yAxis: {
				title: {
					text: null
				}
			},
			plotOptions: {
				column: {
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						style: {
							fontWeight: 'bold'
						},
						formatter: function() {
							return (parseInt((this.y/total) * 1000)/10) +'%';
						}
					},
					point: {
						events: {
							mouseOver: function(event) {
								var row = $('#'+options.legend_container+' table.chartTable tbody tr[data-point='+this.x+']');
								row.trigger('mouseover', event);
							},
							mouseOut: function(event) {
								var row = $('#'+options.legend_container+' table.chartTable tbody tr[data-point='+this.x+']');
								row.trigger('mouseout', event);
							},
							 click: function(event) {
							 	var rows = $('#'+options.legend_container+' table.chartTable tbody tr.over-permanent');
							 	var row = $('#'+options.legend_container+' table.chartTable tbody tr[data-point='+this.x+']');
							 	if ((row.hasClass('over-permanent') && rows.length > 0)) {
							 		rows.removeClass('over-permanent');
							 	} else {
							 		rows.removeClass('over-permanent');
							 		row.toggleClass('over-permanent');
							 	}
							 }
						}
					}
				}
			},
			tooltip: {
				formatter: function() {
					return this.x +':<strong>'+ accounting.formatMoney(this.y) +'</strong>';
				},
				enabled: true
			},
			legend: {
				enabled: false
			},
			series: [{
				name: options.legend_container,
				id: options.legend_container,
				type: 'column',
				data: cdata
			}],
			exporting: {
				enabled: false
			}
		});
		return chart;
		} catch (e) {}
	}
};

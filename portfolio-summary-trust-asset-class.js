function GetURLParameter(sParam){
	var sPageURL = window.location.search.substring(1),
		sURLVariables = sPageURL.split('&');

	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}

var trustAssetClass = (function(){
	function colorShade(hex, lum) {
		hex = String(hex).replace(/[^0-9a-f]/gi, '');
		if (hex.length < 6) {
			hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
		}
		lum = lum || 0;

		var rgb = '#', c, i;

		for (i = 0; i < 3; i++) {
			c = parseInt(hex.substr(i*2,2), 16);
			c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
			rgb += ('00'+c).substr(c.length);
		}

		return rgb;
	}
	function rgbToHex(rgb) {
		rgbArr = rgb.replace('rgb(','').replace(')','').split(','),
			r = parseInt(rgbArr[0]),
			g = parseInt(rgbArr[1]),
			b = parseInt(rgbArr[2]);

		return '#' + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
	}

	function renderAssetClassChart(pie_container, assetdata){
		VCharts.miniPie({
			data: assetdata,
			render_to: pie_container,
			legend_total: false,
			hc: {
				chart: {
					height: (pie_container.indexOf('large') > -1 ? 200 : 140),
					width: (pie_container.indexOf('large') > -1 ? 200 : 140)
				},
				plotOptions: {
					pie: {
						size: (pie_container.indexOf('large') > -1 ? 200 : 130),
						point: {
							events: {
								mouseOver: function(event) {
									var parent = $('[id="' + pie_container + '"]').closest('.chartContainer');
									var class_assets = parent.find('[data-class-name="' + this.name + '"]').show();
									class_assets.find('tr:first td').css("color", this.color);
									parent.find('[id="asset-class-pie-hover"]').stop(true,true).show();
								},
								mouseOut: function(event) {
									var parent = $('[id="' + pie_container + '"]').closest('.chartContainer');
									parent.find('[id="asset-class-pie-hover"]').hide();
									parent.find('[data-class-name="' + this.name + '"]').hide();
								}
							}
						}
					}
				}
			}
		});
	}

	function calcData($table){
		var totalBalance = 0,
			container = $table.closest('.chartContainer').find('.trust_chart_container').attr('id'),
			newData = [],
			lastColor = null,
			iColor = 1;

		// Find all Tier1 Assets (except expanded) and visible Tier2
		$table.find('tr td.charticon').each(function(i,tr){
			var $assetClass = $(this),
				value = $assetClass.find('span').data('value'),
				name = $assetClass.next().find('span').text().trim()
				color = $assetClass.find('span').css('backgroundColor'),
				hexColor = rgbToHex(color);

			if($assetClass.hasClass('tier2')){
				// If current color and last color are the same, increment iColor and determine
				// new color with shade variance
				if(hexColor == lastColor){
					iColor++;
					hexColor = colorShade(hexColor, (iColor * -0.1));
					$assetClass.find('td:first span').css('backgroundColor', hexColor);
				} else {
					// Reset icrement
					iColor = 1;
					lastColor = hexColor;
				}
			}

			if(value != undefined) {
				newData = newData.concat({
					account : name,
					balance : value,
					color : hexColor
				});

				totalBalance += parseFloat(value);
			}
		});

		newData = newData.concat({
			total : totalBalance
		});

		renderAssetClassChart(container, newData);
	}

	function calcDataSimple($table){
		var totalBalance = 0,
			container = $table.closest('.chartContainer').find('.trust_chart_container').attr('id'),
			newData = [],
			lastColor = null,
			iColor = 1;

		// Find all Tier1 Assets (except expanded) and visible Tier2
		$table.find('tbody').each(function(i,tr){
			var $tbody = $(this),
				value = $tbody.data('value'),
				name = $tbody.find('tr:first td').eq(1).text().trim(),
				color = $tbody.find('tr:first td:first span').css('backgroundColor');

			if(name == 'Unclassified'){
				return false;
			}
			if(value !== undefined) {
				newData = newData.concat({
					account : name,
					balance : value,
					color : color
				});

				totalBalance += parseFloat(value);
			}
		});

		newData = newData.concat({
			total : totalBalance
		});

		renderAssetClassChart(container, newData);
	}

	return{
		uninvestedCash : function(){
			calcData($('#trust-assets').find('tbody'));
		}
	};
}());
$(document).on('tabLoadComplete', function(){
	if (GetURLParameter('uninvested_cash') == 'true'){
		$('.uninvested_cash').removeClass('hide');
		$('.invested_cash').remove();
	} else {
		$('.uninvested_cash').remove();
	}
	setTimeout(function() {
        trustAssetClass.uninvestedCash();
    }, 500);
	
});

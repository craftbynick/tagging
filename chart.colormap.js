(function() {
	var ColorMap = function(name) {
		if (name == 'Equities' || name == 'Equity') {
			return '#0052A3';
		} else if (name == 'Brokerage') {
			return '#1A6EBF';
		} else if (name == 'Fixed Income') {
			return '#658D1B';
		} else if (name == 'Cash/Equivalents') {
			return '#6e6259';
		} else if (name == 'Guaranteed') {
			return '#00A0Af';
		} else if (name == 'International Equities') {
			return '#ADCDEA';
		} else if (name == 'Money Market') {
			return '#7F3785';
		} else if (name == 'Multi-Asset') {
			return '#512D6D';
		} else if (name == 'Real Estate') {
			return '#EBAE1D';
		} else if (name == 'Short-Term Fixed Income') {
			return '#BBD890';
		} else if (name == 'Other') {
			return '#484A4D';
		} else if (name == 'Employee') {
			return '#222288'; // old #005EB8';
		} else if (name == 'Employer') {
			return '#6666CC'; // old '#337EC6';
		}
		return false;
	};
	window.ColorMap = ColorMap;
})();

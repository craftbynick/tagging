// Global Utility Functions
(function(a){a.fn.serializeObject=function(){var b={};var c=this.serializeArray();a.each(c,function(){if(b[this.name]!==undefined){if(!b[this.name].push){b[this.name]=[b[this.name]]}b[this.name].push(this.value||"")}else{b[this.name]=this.value||""}});return b};a.unserialize=function(a){var b=decodeURI(a).replace(/\+/," ");var c=b.split("&");var d={},e,f,g;for(var h=0,i=c.length;h<i;h++){e=c[h].split("=");f=e[0];if(f.indexOf("[]")==f.length-2){var j=f.substring(0,f.length-2);if(d[j]===undefined){d[j]=[]}d[j].push(e[1])}else{d[f]=e[1]}}return d}})(jQuery);

window.params = $.unserialize(window.location.search.substr((window.location.search.substr(0, 1) == '?' ? 1 : 0)));

(function($) {
	var newWindow = function(contents, windowname, windowsize) {
		var windowFeatures = windowsize + ",directory=no,location=no,menubar=no,resizable=yes,status=no,toolbar=no,scrollbars=yes";
		popupWindow = window.open(contents, windowname, windowFeatures);
		try {
			popupWindow.focus();
		} catch (error) {
			//this case is for the IE6 issue where subsequent popup after a pdf popup has a JS error- member not found
			popupWindow.close();
			popupWindow = window.open(contents, windowname, windowFeatures);
			popupWindow.focus();
		}
	};

	window.newWindow = newWindow;

	var CreateModal = function(container, width) {
		var modal = $(container).removeClass('hidden').dialog({
			autoOpen: false,
			closeOnEscape: false,
			dialogClass: 'modal',
			draggable: false,
			hide: {
				effect: "fade",
				duration: 600
			},
			minHeight: 20,
			modal: true,
			open: function(event, ui) {
				$('div.ui-dialog-titlebar').addClass('head');
				$('span.ui-dialog-title').addClass('popup-title');
				$('a.ui-dialog-titlebar-close').addClass('popup-close');
				//$('div.ui-dialog-content').addClass('body border');
				$('div.ui-draggable').addClass('popup-draggable');
				$('span.ui-icon').addClass('popup-close-icon');
				if ($('div.ui-dialog:visible').hasClass('modal')) {
					$('div.ui-widget-overlay').addClass('modal-overlay');
				}
				var $this = $(this);
				$this.find('a[href="#close"]').click(function(event) {
					event.preventDefault();
					$this.dialog('close');
				});
			},
			resizable: false,
			show: {
				effect: "fade",
				duration: 600
			},
			width: width
		});
		return modal;
	};

	window.CreateModal = CreateModal;
})(jQuery);

function paychecksRemaining() {
	var differenceMS,payPeriod;
	payPeriod = 1000 * 60 * 60 * 24 * 14;
	differenceMS = Math.abs(Date.parseExact("12/31/"+Date.today().toString('yyyy'),"M/d/yyyy") - Date.today());
	return Math.round(differenceMS/payPeriod);
}

function resetForms(formObjs) {
	var fl = formObjs.length;
	for(f=0;f<fl;f++) // reset on onload
	{
		formObjs[f].reset();
	}
}

function resetFormEls(obj) {
	var thisEl;
	obj.find(':input').each( function() {
		thisEl = $(this);
		switch(thisEl.attr('type')) {
			case 'password':
			case 'textarea':
			case 'text':
				thisEl.val('');
				break;
			case 'select-multiple':
			case 'select-one':
				thisEl.find('option:first').attr('selected','selected');
				break;
			case 'checkbox':
			case 'radio':
				thisEl.attr('checked',false);
				break;
		}
	});
}

function initClick(el) {
	// simulate mouseclick:
	// initClick($('#initSort').get(0));

	if(document.createEvent) {
		var evt = document.createEvent('MouseEvents');
		evt.initMouseEvent('click', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
		var canceled = !el.dispatchEvent(evt);
	} else if(document.createEventObject) {
		var evt = document.createEventObject();
		evt.bubbles = true;
		evt.cancelable = false;
		el.fireEvent('onclick', evt);
	}
}

function getzIndex() {
	// for performance, assumes highest z is on a div
	var t;
	var z = 0;
	$('div').each( function() {
		t = $(this).css('zIndex')*1;
		z = t > z ? t : z;
	});
	return z;
}

function getCookie(name) {
	if(document.cookie.length>0) {
		var c_start=document.cookie.indexOf(name + "=");
		if(c_start!=-1) {
			c_start = c_start + name.length+1;
			var c_end = document.cookie.indexOf(";",c_start);
			if(c_end==-1)
				c_end=document.cookie.length;
			return unescape(document.cookie.substring(c_start,c_end));
		}
	}
	return "";
}

function setCookie(name,value,expiredays) {
	var exdate=new Date();
	exdate.setDate(exdate.getDate()+expiredays);
	document.cookie = name+"="+escape(value)+((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
}

function pad(number,length) {
	var str = '' + number;
	while(str.length < length) {
		str = '0' + str;
	}
	return str;
} 

function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

function addComma(value) {
	var delimiter = ",";
	var a = value.split('.',2)
	var d = a[1];
	var i = parseInt(a[0]);
	if(isNaN(i)) {
		return '';
	}

	var minus = '';
	if(i < 0) {
		minus = '-';
	}
	i = Math.abs(i);
	var n = new String(i);
	var a = [];
	var nn = '';
	while(n.length > 3) {
		nn = n.substr(n.length-3);
		a.unshift(nn);
		n = n.substr(0,n.length-3);
	}
	if(n.length > 0) {
		a.unshift(n);
	}
	n = a.join(delimiter);
	if(d.length < 1) {
		value = n;
	} else {
		value = n + '.' + d;
	}
	value = minus + value;
	return value;
}

function getSSN(value,format) {
	var filtered = String(value).replace(/[^0-9]+/g,'');
	return filtered;
}

function getDateValue(value,format) {
	var filtered = String(value).replace(/[^0-9]+/g,'');
	if(format == "date") {
		filtered = Math.max(filtered,1);
		filtered = Math.min(filtered,31);
		return pad(filtered,2);
	} else if(format == "month") {
		filtered = Math.max(filtered,1);
		filtered = Math.min(filtered,12);
		return pad(filtered,2);
	} else if(format == "year") {
		return filtered;
	}
}

function getNumber(value,format) {
	var filtered = String(value).replace(/[^0-9.]+/g,'');
	if(filtered == '' || format == '' || format == 'numeric') {
		return filtered*1;
	} else if(format == "dollars" && parseFloat(filtered).toFixed(2)) {
		return addComma(parseFloat(filtered).toFixed(2));
	} else if(format == "dollarsWhole" && parseFloat(filtered).toFixed(2)) {
		return addComma(parseFloat(filtered).toFixed(2)).replace(/[.][0-9]+$/g,'');
	} else if(format == "percent") {
		return Math.min(filtered,100);
	} else if(format == "zipcode" || format == "phone") {
		return zipit(filtered.replace(/[.,-]+/g,''));
	} else if(format == "whole") {
		return Math.floor(filtered);
	} else if(format == "numericOnly") {
		return filtered;
	}
}

function get_mmddyyyy(value) {
	var filtered = String(value).replace(/[^0-9/]+/g,'');
	return filtered;
}

function zipit(value) {
	var vl = value.length;
	if(vl > 5) {
		var zip = value.substr(0,5);
		var zipPlus4 = value.substr(5,4);
		value = zip + '-' + zipPlus4;
	}
	return value;
}

// create array of url vars (?first=one&amp;second=two)
function getUrlVars() {
	var vars = [],hash;
	var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

	for(var i=0;i<hashes.length;i++) {
		hash = hashes[i].split('=');
		vars.push(hash[0]);
		vars[hash[0]] = hash[1];
	}
	return vars;
}

function qp(name) // extract query parameters from GET URL string
{
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regexS = "[\\?&]"+name+"=([^&#]*)";
	var regex = new RegExp( regexS );
	var results = regex.exec(window.location.href);
	if(results == null) {
		return "";
	} else {
		return results[1];
	}
}

function iH(id,html) // insert html
{
	if($('#'+id)) {
		$('#'+id).html(html);
	}
}

function iN(formName,inputName,value) // insert value
{
	if(document.forms[formName]) {
		document.forms[formName].elements[inputName].value = value;
	}
}

function iV(id,value) // insert value
{
	if($('#'+id)) {
		$('#'+id).val(value);
	}
}

function ltrim(string) {
	return string.replace(/^\s+/,"");
}

function rtrim(string) {
	return string.replace(/\s+$/,"");
}

function truncate(phrase,length) {
	length = length || 30;
	phrase = phrase === undefined ? '...' : phrase;
	return phrase.length > length ? phrase.slice(0, length - phrase.length) + '...' : phrase;
} 

function setTab(link, event) {
	if(event) {
		event.preventDefault();
	}
	var ulobj = $(link).closest('ul');

	$(ulobj).find('a').removeClass('selected');
	$(link).addClass('selected').blur();

	var duration = ulobj.hasClass('fading') ? 'slow' : 0;
	$('a', ulobj).each(function(i, item) {
		var tabobj = $(item).attr('rel');
		$(tabobj).hide();
	});
	$($(link).attr('rel')).fadeTo(duration, 1);
}

// An implementation of a case-insensitive contains pseudo
// made for all versions of jQuery
(function( $ ) {

/*
$.extend($.expr[':'], {
  'icontains': function(elem, i, match, array) {
    return (elem.textContent || elem.innerText || '').toLowerCase()
    .indexOf((match[3] || '').toLowerCase()) >= 0;
  }
});
*/


function icontains( elem, text ) {
    return (
        elem.textContent ||
        elem.innerText ||
        $( elem ).text() ||
        ""
    ).toLowerCase().indexOf( (text || "").toLowerCase() ) > -1;
}

$.expr[':'].icontains = $.expr.createPseudo ?
    $.expr.createPseudo(function( text ) {
        return function( elem ) {
            return icontains( elem, text );
        };
    }) :
    function( elem, i, match ) {
        return icontains( elem, match[3] );
    };


})(jQuery);


$("ul.tabs li > a, ul.vtabs li > a").live('click', function(event) {
	setTab(this, event);
});

(function( $ ) {
	function createPopup(popupElements) {
	"use strict";
	
	popupElements.each(function(){
	  var popup,popupCustomOptions,popupData,popupDefaultOptions,popupOptions,popupField,popupId;
	
	  popup = $(this); 
	  popupId = popup.attr('id');
	
	  // specify default popup dialog options
	  popupDefaultOptions = {
			  autoOpen:false,
			  close:function(){
				if(popup.data('openerObj') !== '')
				  { popup.data('openerObj').focus(); } // return focus to opening button
				$('#container').attr('aria-hidden','false').removeClass('visible');
				},
			  closeOnEscape:true,
			  closeText:'Close',
			  dialogClass:'modal', // standard class
			  draggable:false,
			  hide:{effect:"fade",duration:600},
			  minHeight:20,
			  modal:true,
			  open:function(){
				popup.find('div.body:first-child').prepend('<a href="#popupContent" style="position:absolute;outline:none;text-decoration:none;">&#160;<!-- empty anchor to prevent initial focus from opening tipLink element, but allow tabbing --></a>'); // in case tipLink is first focusable element
				//configurePopup(); // this doesn't exist and is causing issues; 2/28/14 - JW
				$('#container').attr('aria-hidden','true').css({display:'block',fontSize:'13px',height:'auto',overflow:'auto',visibility:'visible'});
				},
			  resizable:false,
			  show:{effect:"fade",duration:600},
			  width:600
			  };
	
	  // get optional custom popup options specified in data-popup attribute
	  // format = data-popup="width:200,draggable:true"
	  popupCustomOptions = {};
	  if(popup.is('[data-popup]'))
		{
		popupData = popup.attr('data-popup').split(',');
		$.each(popupData,function(){
		  popupField = this.split(':');
		  popupCustomOptions[popupField[0]] = popupField[1];
		});
		}
	  // combine default and custom options (custom values override default values)
	  popupOptions = $.extend({},popupDefaultOptions,popupCustomOptions);
	
	  // add values to adjust for css-defined borders
	  //   makes width/height calculations unnecessary for developer
	  popupOptions.width = parseInt(popupOptions.width,10) + 12;
	  if(popupOptions.height)
		{ popupOptions.height = parseInt(popupOptions.height,10) + 42; }
	
	  // configure popup 
	  popup.data('openerObj',''); // initialize
	  popup.dialog(popupOptions);
	  // jquery update required this addition (removed zIndex):
	  //popup.closest('div.ui-dialog').css({'zIndex':getzIndex()+10}); // ensure popup is above nav, charts
	
	  // add event to anchor/input to open popup (href = popup '#'+id)
	  $('a[href="#'+popupId+'"],input[data-behavior="'+popupId+'"],button[data-behavior~="'+popupId+'"]').on('click keypress',function(event) {
		if(event.type !== 'keypress' || event.which === 13) // allow tabbing
		  {
		  var thisObj = $(this);
		  if(thisObj.is('a')) { event.preventDefault(); } // prevent anchor behavior
		  thisObj.blur();
	
		  popup.data('openerObj',thisObj); // track for accessibility: return focus on close
		  popup.dialog('open');
		  } // end accessibility
	  });
	
	}); // end each popup
	
	$('a.clickHelp,a.clickHelpLink').on('click keypress',function(){
	  if(event.type !== 'keypress' || event.which === 13)
		{
		event.preventDefault();
		var thisClick = $(this);
		thisClick.blur();
		} // end accessibility
	  });
	
	} // end create popup
	
	// ++++++++++++++++++++++++
	// popups
	// create popups dynamically; if adding popup after initialization, call function
	
	if($('div.popup,form.popup').length > 0)
	{
		createPopup($('div.popup,form.popup'));
	}
})( jQuery );

(function( $ ) {
	function initComboBox(){
	"use strict";
	var options,selected,source,thisOption;
	
	source = $('select.comboBox-source');
	selected = source.find('option:selected');
	options = $('option',source);
	
	$('div.comboBox-inner').append('<dl class="comboBox"></dl>');
	$('dl.comboBox').append('<dt><a href="#href"><span class="icon"></span>' + selected.text() + '<span class="value">' + selected.val() + '</span></a></dt>');
	$('dl.comboBox').append('<dd><ul></ul></dd>');
	
	options.each(function(index){
	  thisOption = $(this);
	  $('dl.comboBox dd ul').append('<li><a href="#h'+index+'">' + thisOption.text() + '<span class="value">' + thisOption.val() + '</span></a></li>');
	});
	
	$('dl.comboBox dt a').on('click keypress',function(event) {
	  if(event.type !== 'keypress' || event.which === 13)
		{
		event.preventDefault();
		$('dl.comboBox dd ul').toggle();
		} // end accessibility
	});
	
	$('body').on('click',function(event) {
	  if(!$(event.target).parents().hasClass('comboBox'))
		{ $('dl.comboBox dd ul').hide(); }
	});
	
	$('dl.comboBox dd ul li a').on('click keypress',function(event) {
	  if(event.type !== 'keypress' || event.which === 13)
		{
		event.preventDefault();
		var thisClick;
	
		thisClick = $(this);
		$('dl.comboBox dt a').html(thisClick.html());
		$('dl.comboBox dd ul').hide();
		source.val(thisClick.find('span.value').html());
		} // end accessibility
	});
	
	} // end fun	

	if($('select.comboBox-source').length > 0)
	{
		initComboBox();
	}	
})( jQuery );

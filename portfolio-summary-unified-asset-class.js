if (TIAAPROTO.env.experience === 'unified-asset-class' || TIAAPROTO.env.experience === 'unified-asset-class-v2') {
	var unifiedAssetClass = (function(){
		function colorShade(hex, lum) {
			hex = String(hex).replace(/[^0-9a-f]/gi, '');
			if (hex.length < 6) {
				hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
			}
			lum = lum || 0;

			var rgb = '#', c, i;

			for (i = 0; i < 3; i++) {
				c = parseInt(hex.substr(i*2,2), 16);
				c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
				rgb += ('00'+c).substr(c.length);
			}

			return rgb;
		}
		function rgbToHex(rgb) {
			rgbArr = rgb.replace('rgb(','').replace(')','').split(','),
				r = parseInt(rgbArr[0]),
				g = parseInt(rgbArr[1]),
				b = parseInt(rgbArr[2]);

			return '#' + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
		}

		function renderAssetClassChart(pie_container, assetdata){
			VCharts.miniPie({
				data: assetdata,
				render_to: pie_container,
				legend_total: false,
				hc: {
					chart: {
						height: (pie_container.indexOf('large') > -1 ? 200 : 140),
						width: (pie_container.indexOf('large') > -1 ? 200 : 140)
					},
					plotOptions: {
						pie: {
							size: (pie_container.indexOf('large') > -1 ? 200 : 130),
							point: {
								events: {
									mouseOver: function(event) {
										var parent = $('[id="' + pie_container + '"]').closest('.chartContainer');
										var class_assets = parent.find('[data-class-name="' + this.name + '"]').show();
										class_assets.find('tr:first td').css("color", this.color);
										parent.find('[id="asset-class-pie-hover"]').stop(true,true).show();
									},
									mouseOut: function(event) {
										var parent = $('[id="' + pie_container + '"]').closest('.chartContainer');
										parent.find('[id="asset-class-pie-hover"]').hide();
										parent.find('[data-class-name="' + this.name + '"]').hide();
									}
								}
							}
						}
					}
				}
			});
		}

		function calcData($table){
			var totalBalance = 0,
				container = $table.closest('.chartContainer').find('.unified_chart_container').attr('id'),
				newData = [],
				lastColor = null,
				iColor = 1;

			// Find all Tier1 Assets (except expanded) and visible Tier2
			$table.find('tr.active').each(function(i,tr){
				var $tr = $(this),
					value = $tr.data('value'),
					name = $tr.find('td').first().text().trim()
					color = $tr.find('td:first span').css('backgroundColor'),
					hexColor = rgbToHex(color);

				if($tr.hasClass('tier2')){
					// If current color and last color are the same, increment iColor and determine
					// new color with shade variance
					if(hexColor == lastColor){
						iColor++;
						hexColor = colorShade(hexColor, (iColor * -0.1));
						$tr.find('td:first span').css('backgroundColor', hexColor);
					} else {
						// Reset icrement
						iColor = 1;
						lastColor = hexColor;
					}
				}

				newData = newData.concat({
					account : name,
					balance : value,
					color : hexColor
				});

				totalBalance += parseFloat(value);
			});

			newData = newData.concat({
				total : totalBalance
			});

			renderAssetClassChart(container, newData);
		}

		function calcDataSimple($table){
			var totalBalance = 0,
				container = $table.closest('.chartContainer').find('.unified_chart_container').attr('id'),
				newData = [],
				lastColor = null,
				iColor = 1;

			// Find all Tier1 Assets (except expanded) and visible Tier2
			$table.find('tbody').each(function(i,tr){
				var $tbody = $(this),
					value = $tbody.data('value'),
					name = $tbody.find('tr:first td').eq(1).text().trim(),
					color = $tbody.find('tr:first td:first span').css('backgroundColor');

				if(name == 'Unclassified'){
					return false;
				}
				newData = newData.concat({
					account : name,
					balance : value,
					color : color
				});

				totalBalance += parseFloat(value);
			});

			newData = newData.concat({
				total : totalBalance
			});

			renderAssetClassChart(container, newData);
		}

		return{
			versionOne : function(){
				calcData($('#assetClassProductTiers').find('tbody'));

				$('.show-assets').on('click', function(e){
					e.preventDefault();
					var $this = $(this),
						$tr = $this.closest('tr'),
						$table = $tr.closest('tbody'),
						classType = $tr.data('type');

					$tr.toggleClass('active');
					$table.find('tr.tier2[data-type="'+classType+'"]').toggle().toggleClass('active');
					$this.closest('td').next().find('span').toggle();

					calcData($table);
				});

				$('#changeAssetView').find('li a').on('click', function(e){
					var $this = $(this),
						$other = $this.closest('li').siblings().find('a'),
						selected = $this.attr('href'),
						unselected = $other.attr('href');

					e.preventDefault();
					$(selected).show();
					$(unselected).hide();
					$this.addClass('selected');
					$other.removeClass('selected');
					calcData($(selected).find('tbody'));
				});
			},
			versionTwo : function(){
				$('.show-assets').closest('tr').siblings().hide();
				calcDataSimple($('#assetClassProducts'));
				calcDataSimple($('#assetClassHoldings'));
				$('.tier2').hide();
				$('#changeAssetView').find('li a').on('click', function(e){
					var $this = $(this),
						$other = $this.closest('li').siblings().find('a'),
						selected = $this.attr('href'),
						unselected = $other.attr('href');

					e.preventDefault();
					$(selected).show();
					$(unselected).hide();
					$this.addClass('selected');
					$other.removeClass('selected');
				});
				$('.show-assets').on('click', function(e){
					e.preventDefault();
					$(this).closest('tr').siblings().toggle();
				});
			}
		};
	}());
	$(document).on('tabLoadComplete', function(){
		if($('.versionOne').length){
			unifiedAssetClass.versionOne();
		} else 	if($('.versionTwo').length){
			unifiedAssetClass.versionTwo();
		}
	});
}

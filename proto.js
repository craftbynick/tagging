$(function(){

TIAA = window.TIAA || {};
TIAA.AccountHome = TIAA.AccountHome || {};
TIAA.AccountHome.ThreeSixty = (function(){
	Handlebars.getTemplate = function(name, url) {
		if (Handlebars.templates === undefined || Handlebars.templates[name] === undefined) {
			$.ajax({
				url : url,
				dataType: 'text',
				success : function(response, status) {
					if (Handlebars.templates === undefined) {
						Handlebars.templates = {};
					}
					$(response).filter('script[id="'+name+'"]').each(function() {
						Handlebars.templates[name] = Handlebars.compile($(this).html());
					});
				},
				async : false
			});
		}
		return Handlebars.templates[name];
	};
	function formObj(el){
		var o = {},
		a = el.serializeArray();
		$.each(a, function() {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	}
	function guid(){
		var id = parseInt(Math.random().toString().substr(2, 2)),
			uid = TIAA.AccountHome.ThreeSixty.uid;
		return uid.indexOf(id) < 0 ? id : guid();
	}
	function removeCurrency(num){
		var n = num.replace(/\$|,/g,'');
		if(isNaN(n) || n === null) { return 0; }
		return parseFloat(n);
	}
	return {
		init : function(){
			var self = this;
			TIAAPROTO.utils.session('external_accounts', null, function(data){
				self.ExternalAccounts = data;
			});
			this.uidUpdate();
			this.add360Modal();
		},
		ExternalAccounts : [],
		uid : [],
		uidUpdate : function(){
			var self = this;
			self.uid.length = 0;
			$('li[data-extid]').each(function(){
				self.uid.push($(this).attr('data-extid'));
			});
		},
		Breadcrumb : {
			step1 : function(){
				$('#wizardHeader')
					.find('.breadCrumb span').removeClass('active').addClass('inActive').end()
					.find('.step1-link').hide().end()
					.find('.step1-static').addClass('active').show();

				$('.add-360-step').hide();
				$('.add-360-step1').show();
			},
			step2 : function(){
				$('#wizardHeader')
					.find('.breadCrumb span').removeClass('active').addClass('inActive').end()
					.find('.add-360-step2').addClass('active').end()
					.find('.step1-static').hide().end()
					.find('.step1-link').show();
			},
			step3 : function(){
				$('#wizardHeader')
					.find('.breadCrumb span').removeClass('active').addClass('inActive').end()
					.find('.add-360-step3').addClass('active').end()
					.find('.step1-link').hide().end()
					.find('.step1-static').show();
			}
		},
		add360Modal : function(){
			var self = this;
			// Disable Links
			$('a[href="noLink.html"]').on('click', function(e){
				e.preventDefault();
			});
			$('#manualAccountTypeId').on('change', function(){
				$('#containerFields').show();
			});
			//Return to Step1
			$('.add-360-account, .step1-link').on('click', function(e){
				e.preventDefault();0
				self.Breadcrumb.step1();
			});
			// Move to Manual Account Step 2
			$('.add-manual-account').on('click', function(){
				self.Breadcrumb.step2();
				$('.add-360-step').hide();
				$('.add-360-manual-step2').show();
			});
			// Move to Vanguard Account Step 2
			$('.add-vanguard-account').on('click', function(){
				self.Breadcrumb.step2();
				$('.add-360-step').hide();
				$('.add-360-vanguard-step2').show();
			});
			// Manual Account Form - move to Manual Step 3
			$('#addManualForm').on('submit', function(e){
				var $this = $(this),
					formFieldObj = formObj($this),
					newExternalAccount,
					newId = guid();

				e.preventDefault();
				//Build External Account
				newExternalAccount = self.buildExternalAccount(formFieldObj, newId);
				self.uid.push(newId);

				//Update MyTC & Change Permission
				self.update360Bricklet(newExternalAccount);
				self.updateBalances(newExternalAccount.total);

				//Update Session w/ new External Account
				self.ExternalAccounts.push(newExternalAccount);

				TIAAPROTO.utils.session('external_accounts', JSON.stringify(self.ExternalAccounts));

				// Show Step3 and Hide other Steps
				$('#addedAccountsTable').find('td').eq(1).html(formFieldObj.accountname).end();
				$('#addedAccountsTable').find('td').eq(2).html('$'+formFieldObj.currentbalance).end();
				self.Breadcrumb.step3();
				$('.add-360-step').hide();
				$('.add-360-manual-step3').show();
				$this.trigger('reset');
			});
			// Vanguard Account Form - move to Vanguard Loading
			$('#addVanguardForm').on('submit', function(e){
				var $this = $(this),
					order_num = self.ExternalAccounts.length,
					newExternalAccount,
					newId = guid();

				e.preventDefault();

				newExternalAccount = {
					id : newId,
					name : 'Vanguard-Investments - IRA',
					company : 'Vanguard',
					number : 83187456,
					total : 25000,
					category : 'IRA',
					type : null,
					account_group_id : null,
					icon : 'ui/iwc/360/images/vanguard-icon.ico',
					order: order_num,
					user_id : null,
					has_permission : '1'
				}

				$('.add-360-step').hide();
				$('.add-360-vanguard-loading').show();
				$this.trigger('reset');

				//Update Session w/ new External Account
				self.ExternalAccounts.push(newExternalAccount);
				TIAAPROTO.utils.session('external_accounts', JSON.stringify(self.ExternalAccounts));

				setTimeout(function(){
					$('.add-360-step').hide();
					$('.add-360-vanguard-step3').show();
					setTimeout(function(){
						$('#addedVanguardAccountsTable tr')
							.find('td').eq(2).html('$25,000.00').end().end()
							.find('td').eq(3).html('<span class="accountSuccess succeeded ccell">Added</span> <span class="valign_middle check_mark_16by16_base"></span>');
						$('#gatheringRefreshMsgVanguard').hide();
						$('#successRefreshMsgVanguard').show();
						self.Breadcrumb.step3();

						//Update MyTC & Change Permission
						self.update360Bricklet(newExternalAccount);
						self.updateBalances(newExternalAccount.total);
					}, 3000);
				}, 3000);
			});
			$('.add-360-vanguard-step3 .add-360-account').on('click', function(){
				$('#addedVanguardAccountsTable tr')
					.find('td').eq(2).html('').end()
					.find('td').eq(3).html('<span class="mlxlg"><img width="16" height="16" src="'+TIAAPROTO.env.root+'ui/iwc/360-fv/images/waiting.gif" alt=""></span>');
				$('#gatheringRefreshMsgVanguard').show();
				$('#successRefreshMsgVanguard').hide();
			});
		},
		buildExternalAccount : function(formDetails, newId){
			var liabilitiesArr = ['bills', 'cable_satellite', 'credits', 'loans', 'minutes', 'isp', 'other_liabilities', 'telephone', 'bill_payment', 'utilities'],
				isLiability = null,
				order_num = this.ExternalAccounts.length,
				updateTotal;

			//Determine Liability
			if(liabilitiesArr.indexOf(formDetails.accounttype) >= 0){
				isLiability = 'liability';
				updateTotal = -Math.abs(formDetails.currentbalance);
			} else {
				updateTotal = Math.abs(formDetails.currentbalance);
			}
			//Build and Return Account
			return {
				id : newId,
				name : formDetails.nickname,
				company : formDetails.accountname,
				number : formDetails.accountnumber,
				total : updateTotal,
				category : formDetails.accounttype,
				type : isLiability,
				account_group_id : null,
				icon : null,
				order: order_num,
				user_id : null,
				has_permission : '1'
			};
		},
		update360Bricklet : function(externalAcct){
			var today = new Date(),
				dd = today.getDate(),
				mm = today.getMonth()+1,
				yyyy = today.getFullYear(),
				yy = yyyy.toString().substring(2),
				acctNumStr = externalAcct.number.toString(),
				maskAccount = 'XXXXXXXXX'+acctNumStr.substring(acctNumStr.length - 4),
				templateUrl = TIAAPROTO.env.root + 'application/views/iwc/partials/handlebars/360-fv.handlebars',
				tplBricklet, tplModal, tplAdded, account;

			account = {
				id : externalAcct.id,
				name : externalAcct.name,
				number : maskAccount,
				total : dollarFormatter(externalAcct.total),
				today : pad(mm)+'/'+pad(dd)+'/'+yy,
				type : externalAcct.type,
				icon : TIAAPROTO.env.root+externalAcct.icon
			};

			if(externalAcct.type == 'liability'){
				$('.ext-liability-acct .navList').append('<li class="ntsep" data-extid="'+externalAcct.id+'">'+externalAcct.name+'</li>');
				tplModal = Handlebars.getTemplate('external_accounts_liability-modal', templateUrl);
				$('#modal-grantPermission .acctlist').append(tplModal(account));
			} else {
				tplBricklet = Handlebars.getTemplate('external_accounts', templateUrl);
				tplModal = Handlebars.getTemplate('external_accounts-modal', templateUrl);

				$('.ext-liability-acct').before(tplBricklet(account));
				$('#modal-grantPermission li.acctitem.last:first').before(tplModal(account));
			}

			tplAdded = Handlebars.getTemplate('added_accounts_modal_list', templateUrl);
			$('.added-accounts-modal-list').last().after(tplAdded(account));
		},
		updateBalances : function(balance){
			var $externalAccounts = $('#externalAccounts').find('.current-balance'),
				$outsideAssets = $('#banner-container').find('.balance').last().contents().filter(function() {
					return this.nodeType == 3;
				}),
				$totalPortfolio = $('#total-account-value'),
				externalBalance = removeCurrency($externalAccounts.text()),
				outsideAssetsBalance = removeCurrency($outsideAssets.text()),
				portfolioBalance = removeCurrency($totalPortfolio.text());

			if(balance > 0){
				$externalAccounts.text(dollarFormatter(balance + externalBalance));
				$outsideAssets[0].textContent = dollarFormatter(balance + parseFloat(outsideAssetsBalance));
				$totalPortfolio.text(dollarFormatter(balance + portfolioBalance));
			}
		}
	};
}());
TIAA.AccountHome.ThreeSixty.init();
});
/**
 * Don't execute anything from this file.
 * It should just be a collection of methods.
 * Processing should be called outside somewhere else.
 */
(function() {
	"use strict";

	window.TIAAPROTO = window.TIAAPROTO || {};

	/**
	 * Utilities
	 */
	TIAAPROTO.utils = TIAAPROTO.utils || {};
	TIAAPROTO.utils.modal = function(options) {
		// we need {triggerSelector, targetSelector, success, error, partial: {class, template} }
		if ($(options.triggerSelector).length) {
			if ($(options.targetSelector).length) {
				// refresh it - without closing it if options.silent is true
			} else {
				$.ajax({
					url: TIAAPROTO.env.root + 'partials/' + options.partial['class'] + '/' + options.partial.template,
					dataType: 'html',
					type: 'GET',
					success: function(data, textStatus, jqXHR) {
						$(data).appendTo('body').find('.jq-actions').actionLink();
						TIAA.ui.initModalPopup();
						if ($(options.targetSelector).length && !options.silent) {
							$(options.targetSelector).dialog('open');
						}
						if (typeof options.success == 'function') {
							options.success();
						}
					}
				});
			}
		}
	};

	TIAAPROTO.utils.session = function(key, value, successCallback, errorCallback) {
		var ajax = {
			url: TIAAPROTO.env.root + 'rest/session/' + key,
			success: function(data) {
				if (data.status == 'OK' && typeof successCallback == 'function') {
					successCallback(data.data);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				if (typeof errorCallback == 'function') {
					errorCallback(this.converters['text json'](jqXHR.responseText));
				}
			},
			dataType: 'json'
		};
		if (value === false) {
			ajax.type = 'DELETE';
		} else if (typeof value == 'undefined' || value === null) {
			ajax.type = 'GET';
		} else {
			ajax.data = {
				value: value
			};
			ajax.type = 'POST';
		}
		$.ajax(ajax);
	};

	TIAAPROTO.utils.experience = function(key, value, successCallback, errorCallback) {
		var ajax = {
			url: TIAAPROTO.env.root + 'rest/experience/' + key,
			success: function(data) {
				if (data.status == 'OK' && typeof successCallback == 'function') {
					successCallback(data.data);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				if (typeof errorCallback == 'function') {
					errorCallback(this.converters['text json'](jqXHR.responseText));
				}
			},
			dataType: 'json'
		};
		if (value === false) {
			ajax.type = 'DELETE';
		} else if (typeof value == 'undefined' || value === null) {
			ajax.type = 'GET';
		} else {
			ajax.data = {
				value: value
			};
			ajax.type = 'POST';
		}
		$.ajax(ajax);
	};

})();
/**
 * Don't execute anything from this file.
 * It should just be a collection of methods.
 * Processing should be called outside somewhere else.
 */
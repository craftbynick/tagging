// Chart Utility Functions
function pctFormatter(v, axis) {
	return v + '%';
}

function RIstackHover(event, pos, obj) {
	var eobj = $(event.target);
	var hobj = '#' + event.target.id + '-hover';
	if(obj) {
		var txt;
		switch(obj.datapoint[1]) {
			case 5879:
				return false
				break;

			case 9847:
				txt = '<span class="txts">Payments: $234.23</span>'
				break;

			case 6847:
				txt = '<span class="txts">Lifetime Income: $21,933.93</span>'
				break;
		}
		var hobjpos = (eobj.width() - $(hobj).width() / 2);
		$(hobj).find('span').html(txt);
		$(hobj).css('top', (obj.pageY)).css('left', (obj.pageX + 50)).fadeIn('fast');
	} else {
		$(hobj).fadeOut('fast');
	}
}

function barHover(event, pos, obj) {
	var eobj = $(event.target);
	var hobj = '#' + event.target.id + '-hover';
	if(obj) {
		var hobjpos = (eobj.width() - $(hobj).width() / 2);
		$(hobj).find('span').text(pctFormatter(obj.datapoint[1]));
		var t;
		if(obj.datapoint[1] > 0) {
			t = obj.pageY - $(hobj).height() - 10;
		} else {
			var yP = parseInt(obj.series.yaxis.p2c(obj.datapoint[1]));
			var y0 = parseInt(obj.series.yaxis.p2c(0));
			t = obj.pageY - (yP - y0) - $(hobj).height() - 10;
		}

		$(hobj).css({
			'top' : t + 'px',
			'left' : (obj.pageX) - ($(hobj).width() / 2) + 1 + 'px'
		}).stop(true, true).fadeIn('fast');
	} else {
		$(hobj).fadeOut('fast');
	}
}

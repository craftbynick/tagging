
var componentArray = [];

$(document).ready(function () {


	$("div.title, section.calloutBlue, li.acctitem").mouseover(function() 
	{
		var status = $(this).data('added') || false;
		var content = $(this).html();
        // if status is false then create a new element
        if (!status) 
		{
            var tag = $('<input>', 
			{
				type: 'button',
				value: 'Tagit',
                      'class': 'tagit_click',
					  
				 contentType: 'application/json; charset=utf-8',
                 dataType: 'json',	  
                click: function () 
				{
                    // fetch and save data to JSON from here
					
					componentArray.push({tagTitle:content,id:new Date()});
					console.log(JSON.stringify(componentArray));					
                    var file = JSON.stringify(componentArray);
													 
					$.post( "upone.php", JSON.stringify(componentArray))
					.done(function( data ) 
					{
						console.log( "Data Loaded: " + data );
						componentArray = [];
					});
					
                }
            });
            
			$(this).data('added',true);
			$(this).prepend(tag);
			
		}
    })
	.mouseleave(function() 
	{
		var status = $(this).data('added') || false;
        
        if (status) 
		{
			$(this).data('added',false);
			$(this).find('.tagit_click').remove();		
        }
	});	
	
	
   
});